"""updated timezone of consent_time

Revision ID: e3e08e636fa8
Revises: 229f70b91f0d
Create Date: 2022-10-13 18:05:48.433680

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm


# revision identifiers, used by Alembic.
revision = 'e3e08e636fa8'
down_revision = '4d1e00e86529'
branch_labels = None
depends_on = None

#import lms42.models.grading
import lms42.utils

def upgrade():
    pass
#    bind = op.get_bind()
#    session = orm.Session(bind=bind)
#
#    for grading in session.query(lms42.models.grading.Grading):
#        if grading.consent_time:
#            grading.consent_time = lms42.utils.local_to_utc(grading.consent_time)
#
#    session.commit()


def downgrade():
    pass
#    bind = op.get_bind()
#    session = orm.Session(bind=bind)
#
#    for grading in session.query(lms42.models.grading.Grading):
#        if grading.consent_time:
#            grading.consent_time = lms42.utils.utc_to_local(grading.consent_time)
#
#    session.commit()
