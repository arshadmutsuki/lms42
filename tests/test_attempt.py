from lms42.models.attempt import Attempt
from lms42.models.user import User

def test_submit(client):
    client.login("student1")
    client.open('/curriculum/vars')
    client.find("input", value="Start attempt anyway").submit()

    client.find(text="In progress for 0.0 working days.").require()
    client.find(in_text="You haven't uploaded any work yet.").require()
    client.find("input.button.is-warning", value='Submit attempt ignoring warnings').require()

def test_pause_attempt(client):
    client.login("student1")
    client.open('/curriculum/linux')
    client.find("input", value="Start attempt").submit()

    client.find(text="In progress for 0.0 working days.").require()

    client.open('/curriculum/vars')
    client.find("input", value="Start attempt anyway").submit()

    client.find(text="Attempt paused!").require()

    attempt_started(client)

    client.open('/curriculum/linux')
    client.find(text="This assignment is on pause. Finish your current assignment to resume.").require()


def test_resume_attempt(client):
    test_pause_attempt(client)

    client.open('/curriculum/vars')
    client.find("input", value='Submit attempt ignoring warnings').submit(finished="yes")

    client.open('/curriculum/linux')
    attempt_started(client)


def test_pause_twice(client):
    test_pause_attempt(client)

    client.open('/curriculum/queries')
    client.find(text="You are already working on another assignment and you already have a paused attempt.").require()


def test_pause_exame(client, _db):
    client.login("student1")
    user = User.query.filter_by(short_name="student1").first()
    assert user.current_attempt_id is None

    # Create artificial exam attempt and set student1.current_attempt_id to it
    exam_attempt = Attempt(student_id=user.id, number=1, node_id = 'python-exam', variant_id = 1, status='in_progress', credits=5, avg_days = 1)
    _db.session.add(exam_attempt)
    _db.session.commit()
    user.current_attempt_id = exam_attempt.id
    _db.session.commit()

    client.open('/curriculum/vars')

    client.find(text="You cannot start another assignment while you are working on an exam.").require()

def attempt_started(client):
    client.find(text="In progress for 0.0 working days.").require()
    client.find(in_text="You haven't uploaded any work yet.").require()
    client.find("input.button.is-warning", value='Submit attempt ignoring warnings').require()
