from ..app import db, app
from ..models.user import User, AbsentDay, TimeLog, get_students, STUDENT_LEVEL
from ..models.attempt import Attempt
from ..models import curriculum
from ..utils import role_required
from .. import working_days
from . import discord
import os
import flask
import datetime
import operator
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import sqlalchemy as sa
from flask_login import login_required, current_user


ABSENT_REASONS_DICT = {
    "": "Present",
    "sick": "Sick",
    "home_incidental": "Ad hoc home",
    "absent_incidental": "Ad hoc absent",
    "home_structural": "Structural home",
    "absent_structural": "Structural absent"
}



class FilterForm(flask_wtf.FlaskForm):
    hidden = wtf.BooleanField('Hidden')
    inactive = wtf.BooleanField('Inactive')
    class_name = wtf.SelectField('Class')


@app.route('/people', methods=['GET','POST'])
@login_required
def list_people():
    filter_form = None
    show_hidden = show_inactive = False
    class_filter = '%'
    if current_user.is_teacher:
        filter_form = FilterForm(flask.request.args)
        filter_form.class_name.choices = ['All classes'] + [class_name or "No class" for class_name, in db.session.query(User.class_name).filter_by(is_student=True, is_active=True).distinct(User.class_name)]
        show_hidden = flask.request.args.get('hidden')
        show_inactive = flask.request.args.get('inactive')
        class_filter = flask.request.args.get('class_name')
        if not class_filter or class_filter == 'All classes':
            class_filter = '%'
        elif class_filter == 'No class':
            class_filter = ''
    
    return flask.render_template('people-list.html',
        students = get_students(everybody=True, students_only=False, show_hidden=show_hidden, show_inactive=show_inactive, class_filter=class_filter),
        filter_form = filter_form,
        show_classes = class_filter=='%',
    )


def get_progress_graph(user_id, start_date):
    days_per_month = working_days.calculate_per_month(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())

    month_positions = {month: position for position, month in enumerate(days_per_month)}
    labels = [month for month in days_per_month]

    sql = f"""select month, short_name as name, sum(avg_days) as progress
    from (
        select u.short_name, max(a.avg_days) avg_days, a.node_id, to_char(min(a.submit_time), 'YYYY-MM') as month
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = {int(user_id)}
        group by u.id, a.node_id
    ) as passed
    group by short_name, month
    order by short_name, month
    """

    series = [
        {"name": "progress", "data": [0 for _ in days_per_month]},
        {"name": "hours", "data": [0 for _ in days_per_month]}
    ]
    with db.engine.connect() as dbc:
        for row in dbc.execute(sql):
            month = row['month']
            if month in month_positions:
                series[0]["data"][month_positions[month]] = round(100 * row["progress"] / days_per_month[month])

    if "2021-06" in days_per_month:
        days_per_month["2021-06"] -= 7 # days before time_log started

    for row in User.query.get(user_id).query_hours():
        month = row['period']
        if month in month_positions:
            series[1]["data"][month_positions[month]] = round(100 * row["seconds"] / days_per_month[month] / 8 / 60 / 60)

    #print("get_progress_graph", series, flush=True)
    return {"labels": labels, "series": series}

@app.route('/people/leaderboard', methods=['GET'])
@login_required
def show_leaderboard():

    sql = f"""
        WITH last_week_attempt AS (
            SELECT *
            FROM attempt
            WHERE submit_time BETWEEN :start AND :end
        )
        SELECT
            u.id as uid,
            (
                SELECT COALESCE(SUM(avg_days),0)
                FROM (
                    SELECT DISTINCT a.node_id, a.avg_days
                    FROM last_week_attempt a
                    WHERE a.student_id=u.id AND a.status='passed'
                ) a1
            ) passed,
            (
                SELECT COALESCE(SUM(avg_days),0)
                FROM (
                    SELECT DISTINCT a.node_id, a.avg_days
                    FROM last_week_attempt a
                    WHERE a.student_id=u.id AND a.status='needs_grading'
                ) a2
            ) needs_grading
        FROM "user" u
        WHERE u.level = :student_level and u.is_hidden = False and u.is_active = True
        ORDER BY passed DESC NULLS LAST, needs_grading DESC NULLS LAST
        LIMIT :limit
    """

    db_result = db.session.execute(sql, {
        "start": working_days.offset(datetime.date.today(), -10),
        "end": datetime.date.today(),
        "limit": 99999 if current_user.is_teacher else 10,
        "student_level": STUDENT_LEVEL,
    })
    progress_per_person = []
    for row in db_result:
        if row[1] or row[2] or current_user.is_teacher:
            progress_per_person.append({
                'person': User.query.get(row[0]),
                'passed': round(row[1]),
                'needs_grading': round(row[2])
            })

    return flask.render_template('leaderboard.html', progress_per_person=progress_per_person)

class EditForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[
        wtv.DataRequired()
    ])

    last_name = wtf.StringField('Last name', validators=[
    ])

    email = wtf.StringField('Email', validators=[
        wtv.Email(), wtv.DataRequired()
    ])

    short_name = wtf.StringField('Short name', validators=[
        wtv.DataRequired(), wtv.Regexp('^[a-z0-9\\-]{2,}$', 0, 'Only numbers, lower case English letters and dashes are allowed.')
    ])

    class_name = wtf.StringField('Class', validators=[], default='ESD1V.')

    level = wtf.SelectField('Level', choices = [(10,'Student'), (30,'Inspector'), (50,'Teacher'), (80,'Admin'), (90,'Owner')], default=10, coerce=int)

    counselor_id = wtf.SelectField('Study coach', coerce=(lambda value: None if value=="None" or value==None else int(value)))

    is_active = wtf.BooleanField('Active', default=True)
    is_hidden = wtf.BooleanField('Hidden', default=False)

    choices = [("present","Present"), ("home_structural","Home"), ("absent_structural","Absent")]

    monday = wtf.RadioField("Mon", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    tuesday = wtf.RadioField("Tue", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    wednessday = wtf.RadioField("Wed", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    thursday = wtf.RadioField("Thu", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    friday = wtf.RadioField("Fri", choices=choices, render_kw={"class": "default-schedule"}, default='present')

    submit = wtf.SubmitField('Save')

@app.route('/people/<int:user_id>', methods=['GET','POST'])
@login_required
def user_profile_id(user_id):
    person = User.query.get(user_id)
    if person:
        return flask.redirect(f'/people/{person.short_name}')
    else:
        return flask.render_template('404.html', message = 'No such person'), 404


def get_counselor_choices():
    return [(None, 'None')] + [(c.id, c.full_name) for c in User.query.filter_by(is_teacher=True, is_active=True).all()]


@app.route('/people/<string:short_name>', methods=['GET','POST'])
@login_required
def user_profile(short_name):
    person = User.query.filter_by(short_name=short_name).first()
    if not person:
        return flask.render_template('404.html', message = 'No such person'), 404
    
    privileged = current_user.is_teacher or current_user.id == person.id

    edit_form = None
    if current_user.is_admin and current_user.level >= person.level:
        edit_form = EditForm(obj=person,
            monday = person.default_schedule[0],
            tuesday = person.default_schedule[1],
            wednessday = person.default_schedule[2],
            thursday = person.default_schedule[3],
            friday = person.default_schedule[4]
        )
        edit_form.counselor_id.choices = get_counselor_choices()
        if edit_form.validate_on_submit():
            try:
                if edit_form.level.data > current_user.level:
                    # Users cannot give anyone a level that is higher than their own
                    edit_form.level.data = max(person.level, current_user.level)

                edit_form.populate_obj(person)

                if not person.is_active:
                    if person.current_attempt_id:
                        person.current_attempt.status = "needs_grading"
                    person.current_attempt_id = None
    
                schedule = [edit_form.monday.data, edit_form.tuesday.data, edit_form.wednessday.data, edit_form.thursday.data, edit_form.friday.data]
                person.default_schedule = schedule
                db.session.commit()
                return flask.redirect('/people')
            except sa.exc.IntegrityError:
                db.session.rollback()
                edit_form.email.errors.append('Email address already in use.')
        
    performance = person.performance if privileged else None
    # TODO: These two should probably also be moved to Performance:
    graph = get_progress_graph(person.id, person.start_date) if privileged else None
    attempts = person.get_attempts() if privileged else None

    absent_reason = AbsentDay.query \
        .with_entities(AbsentDay.reason) \
        .filter(AbsentDay.date==datetime.date.today()) \
        .filter(AbsentDay.user_id==person.id) \
        .scalar() or ""

    discord_url = ''
    discord_status = ''
    if os.environ.get("DISCORD_CLIENT_ID") and current_user.id == person.id:
        if person.discord_id:
            discord_status = 'authorized'
        else:
            discord_status = 'unauthorized'
            discord_url = discord.get_auth_url()

    return flask.render_template('people-detail.html',
        person=person,
        absent_reason=absent_reason,
        absent_reasons_dict=ABSENT_REASONS_DICT,
        edit_form=edit_form,
        update_avatar=privileged,
        graph=graph,
        attempts=attempts,
        performance=performance,
        discord_status=discord_status,
        discord_url=discord_url
    )


@app.route('/people/new', methods=['GET','POST'])
@login_required
def new():
    if not current_user.is_admin:
        return 'Only admins can do that.', 403

    edit_form = EditForm()
    edit_form.counselor_id.choices = get_counselor_choices()
    if edit_form.validate_on_submit():
        try:
            person = User()
            edit_form.populate_obj(person)
            db.session.add(person)
            db.session.commit()
            return flask.redirect(f"/people/{person.id}")
        except sa.exc.IntegrityError:
            db.session.rollback()
            edit_form.email.errors.append('Email address already in use.')

    return flask.render_template('people-add.html', person={}, edit_form=edit_form)


@app.route('/people/<int:user_id>/avatar', methods=['POST'])
@login_required
def upload_avatar(user_id):
    if current_user.short_name in ["robin", "indy"]:
        flask.flash("Nope!")
    elif current_user.is_teacher or current_user.id == user_id:
        user = User.query.get(user_id)
        user.avatar = flask.request.form['avatar']
        db.session.commit()
    else:
        flask.flash("Permission denied.")
    return flask.redirect(f"/people/{user_id}")


@app.route('/people/<int:user_id>/needs_check_in', methods=['POST'])
@role_required('teacher')
def set_needs_check_in(user_id):
    user = User.query.get(user_id)
    user.needs_check_in = datetime.datetime.now() if flask.request.form.get("needs_check_in") is not None else None
    db.session.commit()
    return "OK"
