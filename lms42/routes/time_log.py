from ..app import db, app, retry_commit
import flask
from flask_login import current_user
import datetime
import sqlalchemy

CHECK_IN_DEADLINE = datetime.time(10,30)

TIME_LOG_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time)
values(:user_id, :date, :time, :time)
on conflict (user_id, date)
do update set end_time = :time
returning start_time
""")

NEEDS_CHECK_IN_QUERY = sqlalchemy.text("""
update "user"
set needs_check_in = now()
where id=:user_id and level=10
""")

SUSPICIOUS_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time, suspicious)
values(:user_id, :date, '7:00', '7:00', true)
on conflict (user_id, date)
do update set suspicious = true
""")

@app.route('/time_log/ping', methods=['GET'])
@retry_commit
def add_time_log():
    now = datetime.datetime.now().replace(microsecond=0)
    ip = flask.request.headers.get('X-Forwarded-For', flask.request.remote_addr)

    if not current_user or not current_user.is_authenticated:
        return {"error": "Not logged in"}
    elif not ip.startswith("145.76.") and not ip.startswith("::ffff:145.76.") and ip != "127.0.0.1" and ip != "::1":
        return {"error": f"Not a Saxion IP ({flask.request.remote_addr})"}
    elif now.hour < 7 or now.hour >= 22:
        with db.engine.connect() as dbc:
            dbc.execute(SUSPICIOUS_QUERY, user_id=current_user.id, date=now.date())
        print(f"Time log outside time window: user_id={current_user.id} now={now}")
        return {"error": f"Outside 7:00-22:00 time window"}
    else:
        time = now.time()
        with db.engine.connect() as dbc:
            start_time = dbc.execute(TIME_LOG_QUERY, user_id=current_user.id, date=now.date(), time=time).fetchone()[0]
            if start_time == time and time <= CHECK_IN_DEADLINE:
                dbc.execute(NEEDS_CHECK_IN_QUERY, user_id=current_user.id)
        return {"success": True}

