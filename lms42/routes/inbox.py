from ..app import db, app
from ..assignment import Assignment
from ..models import curriculum
from ..models.attempt import Attempt, Grading
from ..models.user import User, TimeLog, get_students
from ..utils import role_required, utc_to_display, local_to_utc
from flask_login import  current_user
import flask
import random
import sqlalchemy as sa
import datetime


@app.route('/inbox', methods=['GET'])
@role_required('teacher')
def get_inbox():
    return flask.render_template('inbox.html',
        students = get_students(),
        show_classes = True,
    )


@app.route('/inbox/grade/<attempt_id>', methods=['POST'])
@role_required('teacher')
def grading_post(attempt_id):
    form = flask.request.form

    attempt = Attempt.query.get(attempt_id)

    if attempt.status == "in_progress":
        flask.flash("Attempt is still in progress!?")
        return flask.redirect("/inbox")
    if attempt.status != "needs_grading":
        flask.flash("Attempt was already graded! Your grading has replaced the old grading, but the old grading is still available in the database.")

    ao = Assignment.load_from_directory(attempt.directory)
    scores = ao.form_to_scores(form)
    motivations = ao.form_to_motivations(form)

    grade, passed = ao.calculate_grade(scores)

    # All exams with 5, 6 and 10 grades need approval, as well as a random 10% of the rest.
    needs_consent = "ects" in ao.node and (grade in [5,6,10] or random.randrange(0,10)==0 or bool(form.get('request_consent')))

    grading = Grading(
        attempt_id = attempt.id,
        grader_id = current_user.id,
        objective_scores = scores,
        objective_motivations = motivations,
        grade = grade,
        grade_motivation = form.get('motivation'),
        passed = passed,
        needs_consent = needs_consent,
    )

    if needs_consent:
        attempt.status = "needs_consent"
    elif "ects" in ao.node:
        attempt.status = "passed" if passed else "failed"
    elif form.get('formative_action') in ["failed","passed","repair"]:
        attempt.status = form.get('formative_action')
    else:
        attempt.status = "passed" if passed else "repair"

    db.session.add(grading)
    db.session.commit()

    attempt.write_json()

    student = attempt.student
    if attempt.status == "passed":
        flask.flash(f"{student.full_name} passed with a {round(grade)}!")
    elif attempt.status == "needs_consent":
        flask.flash("Grading needs consent from another teacher.")
        grading.send_consent_dm() # Send a dm to teachers to give grading consent. 
    elif grade == None:
        flask.flash(f"{student.full_name} has not received a grade.")
    else:
        flask.flash(f"{student.full_name} did NOT pass with a {round(grade)}.")
        grading.send_grade_dm() # Send a dm to the student with the failed grade.
    if "ects" in ao.node and attempt.status != "needs_consent":
        grading.announce()
    
    return flask.redirect(f"/people/{attempt.student_id}#attempts")

