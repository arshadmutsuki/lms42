from ..app import db, app
from ..models.trial import TrialDay, TrialStudent
from ..utils import role_required
import flask
import sqlalchemy
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
from flask_login import current_user
from types import SimpleNamespace

class StudentForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[wtv.DataRequired()])
    last_name = wtf.StringField('Last name', validators=[])
    email = wtf.StringField('Email', validators=[wtv.Email(), wtv.DataRequired()])
    submit = wtf.SubmitField('Invite')


class DayForm(flask_wtf.FlaskForm):
    date = wtf.DateField('Date', validators=[wtv.DataRequired()])
    slots = wtf.IntegerField('Slot count', validators=[], default=2)
    submit = wtf.SubmitField('Save')


class StudentEditForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[wtv.DataRequired()])
    last_name = wtf.StringField('Last name')
    email = wtf.StringField('Email', validators=[wtv.Email(), wtv.DataRequired()])

    invitation_time = wtf.DateTimeField()
    trial_day_date = wtf.DateField(validators=[wtv.Optional()])

    student_id = wtf.IntegerField(validators=[wtv.Optional()])
    submit_date = wtf.DateField(validators=[wtv.Optional()])
    modify_date = wtf.DateField(validators=[wtv.Optional()])
    city = wtf.StringField(validators=[wtv.Optional()])
    enrollment_date = wtf.DateField(validators=[wtv.Optional()])
    enrollment_state = wtf.StringField(validators=[wtv.Optional()])
    admission_state = wtf.StringField(validators=[wtv.Optional()])
    nationality = wtf.StringField(validators=[wtv.Optional()])
    correspondence_city = wtf.StringField(validators=[wtv.Optional()])
    correspondence_country = wtf.StringField(validators=[wtv.Optional()])
    language = wtf.StringField(validators=[wtv.Optional()])
    prior_education = wtf.StringField(validators=[wtv.Optional()])
    phone = wtf.StringField(validators=[wtv.Optional()])

    comment = wtf.TextAreaField(validators=[wtv.Optional()])
    manual_probability = wtf.IntegerField(validators=[wtv.Optional()])
    did_online_intake = wtf.BooleanField()

    save = wtf.SubmitField('Save')


@app.route('/trial/<int:id>/edit', methods=['GET','POST'])
@role_required('teacher')
def edit_trial_student(id):
    student = TrialStudent.query.get(id)
    form = StudentEditForm(obj=student)
    if form.validate_on_submit():
        form.populate_obj(student)
        db.session.commit()
        return flask.redirect('/trial')
    return flask.render_template('generic-form.html', form=form)


@app.route('/trial/<int:id>/<secret>', methods=['GET','POST'])
def trial_schedule(id, secret):
    student = TrialStudent.query.get(id)
    if not student or student.secret != secret:
        flask.flash("Sorry, that URL doesn't look right.")
        return flask.render_template('layout.html')

    if flask.request.method == "POST":
        if "cancel" in flask.request.form:
            date = student.trial_day_date
            student.trial_day_date = None
            db.session.commit()
            student.send_cancel_email(date)
        elif "date" in flask.request.form:
            date = flask.request.form["date"]
            day = TrialDay.query.get(date)
            if not day or len(day.students) >= day.slots:
                flask.flash("Sorry, that day is no longer available.")
            else:
                student.trial_day_date = date
                db.session.commit()
                student.send_scheduled_email()
                flask.flash("A confirmation email with further details has been sent.")
        else:
            flask.flash("Please select a date.")

    available = get_available_dates() if student.trial_day_date==None else None

    return flask.render_template('trial-schedule.html', student=student, available=available)



@app.route('/trial', methods=['GET','POST'])
def trial_list():
    student_form = StudentForm()
    if "email" in flask.request.form and student_form.validate_on_submit():
        try:
            student = TrialStudent()
            student_form.populate_obj(student)
            db.session.add(student)
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()
            flask.flash("Address already exists.")
        else:
            student_form = StudentForm(formdata=None)
            student.send_invite_email()
            flask.flash("Invitation sent.")

    day_form = None
    scheduled = None
    stats = None
    if not current_user.is_anonymous and current_user.is_teacher:
        day_form = DayForm()
        if "date" in flask.request.form and day_form.validate_on_submit():
            day = TrialDay.query.get(day_form.date.data) or TrialDay()
            day_form.populate_obj(day)
            db.session.add(day)
            db.session.commit()

        scheduled = list(get_scheduled_trials())
        
        scheduled.append(SimpleNamespace(
            date = "Unscheduled",
            slots = "",
            students = list(TrialStudent.query.filter_by(trial_day_date=None).order_by(TrialStudent.invitation_time or TrialStudent.submit_date)),
        ))

        stats = {
            "total": 0,
            "enrolled": 0,
            "accepted": 0,
            "estimated": 0,
        }
        for day in scheduled:
            for student in day.students:
                stats["total"] += 1
                if student.enrollment_state == "Aangemeld":
                    stats["enrolled"] += 1
                if student.admission_state == "Toegelaten":
                    stats["accepted"] += 1
                stats["estimated"] += student.probability / 100

    return flask.render_template('trial-list.html', student_form=student_form, day_form=day_form, scheduled=scheduled, stats=stats)


def get_available_dates():
    with db.engine.connect() as dbc:
        return [row["date"] for row in dbc.execute("""
                select td.date as date
                from trial_day td
                where
                    td.date>current_date and
                    (select count(*) from trial_student ts where ts.trial_day_date=td.date) < slots
                order by date
            """)]


def get_scheduled_trials():
    return TrialDay.query.order_by(TrialDay.date).options(sqlalchemy.orm.joinedload(TrialDay.students))
