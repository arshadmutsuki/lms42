(function(){

    let nowE = document.getElementById('sched_now')
    let nextE = document.getElementById('sched_next')
    let timeE = document.getElementById('sched_time')


    if (Notification.permission != 'granted'){
        Notification.requestPermission();
    }

    let timesString = `
    08:30 - 09:00: Relaxed work
    09:00 - 09:15: Kick-off
    09:15 - 10:00: Focussed work
    10:00 - 10:15: Break
    10:15 - 11:00: Focussed work
    11:00 - 11:15: Break
    11:15 - 12:00: Focussed work
    12:00 - 12:30: Lunch break
    12:30 - 12:45: Break
    12:45 - 13:30: Focussed work / Coding session
    13:30 - 13:45: Break
    13:45 - 14:30: Focussed work
    14:30 - 14:45: Break
    14:45 - 15:30: Focussed work
    15:30 - 17:30: Relaxed work
    `;
    
    let timetable = [];
    for(let line of timesString.trim().split("\n")) {
        let [times, description] = line.trim().split(': ');
        times = times.split(' - ');
        timetable.push({start: times[0], end: times[1], description: description});
    }

    function calculateTimeDelta(from, to) {
        from = from.split(':').map(a => parseInt(a));
        to = to.split(':').map(a => parseInt(a));
        return (to[0] - from[0])*60 + to[1]-from[1] + " minutes";
    }

    function play(sample) {
        new Audio(`/static/${sample}.mp3`).play()
    }

    function notify(now){
        new Notification(now.description, {
            body: now.description + " until " + now.end,
            icon: '/static/favicon.svg'
        });
    }

    function updateSchedule() {
        var dateTime = new Date();
        time = ("0"+dateTime.getHours()).slice(-2) + ":" + ("0"+dateTime.getMinutes()).slice(-2);
        timeE.innerText = time;

        let nowIndex = 0;
        while(nowIndex+1<timetable.length && time >= timetable[nowIndex+1].start) {
            nowIndex++;
        }

        now = nowIndex < 0 ? null : timetable[nowIndex];
        next = nowIndex + 1 >= timetable.length ? null : timetable[nowIndex+1];

        nowE.innerText = now ? now.description : '';
        nextE.innerText = next ? next.description+" in "+calculateTimeDelta(time, next.start) : "Next: have a great evening!";

        if (now && time==now.start) {
            if (Notification.permission == 'granted'){
                notify(now);
            }
            let descr = timetable[nowIndex].description;
            if (descr === "Lunch break") play('airhorn');
            else if (descr === "Kick-off") play('welcome');
            else if (descr === "Break") play('bell');
            else play('ring');
        }

        setTimeout(updateSchedule, (60.2 - dateTime.getSeconds())*1000);
    }

    updateSchedule();

    // // Reload every 2h
    // setTimeout(function() {
    //     location.reload();
    // }, 2*60*60*1000)
})();
