#!/bin/bash

# Exit on error!
set -e

if [ "$PROJECT_DIR" = "" ]; then
    # Export all variables that are set after this
    set -a

    PROJECT_DIR=$(cd "$(dirname "$0")"; cd ..; pwd -P)
    SD42_CLI=$PROJECT_DIR/bin/sd42
    CONFIG=$PROJECT_DIR/config.sh
    FLASK_ENV=development
    FLASK_APP=$PROJECT_DIR/lms42.app
    ALEMBIC_CONFIG=$PROJECT_DIR/migrations/alembic.ini
    PATH=$PROJECT_DIR/bin:$PATH:$PROJECT_DIR/node_modules/.bin:$PATH

    # Work-around for https://github.com/python-poetry/poetry/issues/1917
    PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

    if [ ! -f $CONFIG ] ; then
        SESSION_KEY=$(< /dev/urandom tr -dc A-Za-z0-9 | head -c16)
        cat <<EOD > $CONFIG
SESSION_KEY=$SESSION_KEY

HTTP_PORT=8000

# Outgoing email
SMTP_HOST= # smtp.office365.com
SMTP_USER= # a.b.name@example.com
SMTP_PASSWORD=

# Set this to 'yes' to be able to login without email link by prefixing ~ to the email address.
# Eg: '~test@example.com'. Obviously this is for development only.
ALLOW_TILDE_LOGIN=no

# ssh target that 'bin/sd42 deploy' will try to deploy to
DEPLOY_TARGET=lms@sd42.nl:lms42

# In order to post lesson feedback comments to a GitLab issue tracker
GITLAB_FEEDBACK_PROJECT=saxion.nl/42/lms42
GITLAB_FEEDBACK_INSTANCE=https://gitlab.com
GITLAB_FEEDBACK_TOKEN= # Create a project access token and copy it here

# Discord
# - Create a new application on https://discord.com/developers/applications
# - Navigate to the OAuth2 --> General tab
#   - Copy your client id and client secret (you'll need to reset it) from the OAuth2 tab
#     to DISCORD_CLIENT_ID and DISCORD_CLIENT_SECRET
#   - Add the redirect URL for the <baseurl>/discord/authorize endpoint
# - Navigate to the Bot tab
#   - Create a new bot
#   - Make the bot private
#   - Make sure the "server members intent" is turned on
#   - Reset the token and copy it to DISCORD_BOT_TOKEN
# - Navigate to the OAuth2 --> URL Generator tab
#   - Use the OAuth2 URL generator to create a url for the "bot" scope and make sure that the "manage nicknames" permissions is selected.
#   - Visit this URL and authorize the bot.
DISCORD_CLIENT_ID=
DISCORD_CLIENT_SECRET=
DISCORD_BOT_TOKEN=

EOD
    fi

    . $CONFIG

    # Disable export by default
    set +a

    cd $PROJECT_DIR

    $SD42_CLI conditional-poetry-install

    . .venv/bin/activate
elif [ "$1" = "shell" -o "$1" = "" ] ; then
    echo Already in sd42 environment
    exit 1
fi


cd "$PROJECT_DIR"

if [ $# = 0 ] ; then
	cmd=help
else
	cmd="$1"
	shift
fi

case "$cmd" in
    shell)
        exec $SHELL
        ;;

    run)
	    exec "$@"
	    ;;

    prod)
        $SD42_CLI conditional-npm-install
        $SD42_CLI watch-npm-install &

        $SD42_CLI conditional-poetry-install
        $SD42_CLI watch-poetry-install &

        $SD42_CLI conditional-db-upgrade
        $SD42_CLI watch-db-upgrade &

        $SD42_CLI insert-nodes
        $SD42_CLI watch-nodes &

        export FLASK_ENV=production
        exec hupper -m waitress --listen="*:${HTTP_PORT:=8000}" lms42:app.app
        ;;

    dev)
        # kill background jobs on exit
        trap 'kill $(jobs -p); exit' EXIT

        $SD42_CLI conditional-npm-install
        $SD42_CLI conditional-poetry-install
        $SD42_CLI conditional-db-upgrade
        
        $SD42_CLI watch-sass &

        $SD42_CLI build-curriculum --ignore-fatal
        $SD42_CLI watch-curriculum &
        
        $SD42_CLI insert-nodes
        $SD42_CLI watch-nodes &

        while true ; do
            hupper -m waitress --listen="127.0.0.1:${HTTP_PORT:=8000}" lms42:app.app || true
            echo Restarting in 2 seconds...
            sleep 2
        done

        ;;

    npm-install)
        echo Install node packages...
        npm ci --cache .npm --prefer-offline
        touch data/timestamps/npm
        ;;

    conditional-npm-install)
        [ data/timestamps/npm -nt package.json ] || exec $SD42_CLI npm-install
        ;;

    watch-npm-install)
        set +e
        while true ; do
            sleep 1
            $SD42_CLI conditional-npm-install
        done
        ;;


    poetry-install)
        echo Installing Python packages...
        poetry install --no-root
        touch data/timestamps/poetry
        ;;

    conditional-poetry-install)
        [ data/timestamps/poetry -nt pyproject.toml ] || exec $SD42_CLI poetry-install
        ;;

    watch-poetry-install)
        set +e
        while true ; do
            sleep 1
            $SD42_CLI conditional-poetry-install
        done
        ;;

    watch-sass)
        exec npx sass --watch css/style.scss lms42/static/style.css
        ;;

    insert-nodes)
        exec python bin/insert-nodes.py "$@"
        ;;

    watch-nodes)
        exec npx onchange --await-write-finish 100 curriculum.pickle -- $SD42_CLI insert-nodes
        ;;

    build-curriculum)
        echo Building curriculum...
        python bin/build-curriculum.py "$@"
        python bin/build-exam-changes.py
        ;;

    watch-curriculum)
        exec npx onchange -k --await-write-finish 100 'curriculum/**' 'bin/*' --exclude-path .gitignore -- $SD42_CLI build-curriculum --ignore-fatal
        ;;

    db-revision)
        exec alembic revision --autogenerate
        ;;

    db-upgrade)
        echo Upgrading database schema...
        flask db upgrade
        touch data/timestamps/db-update
        ;;

    conditional-db-upgrade)
        [ data/timestamps/db-update -nt migrations/versions ] || exec $SD42_CLI db-upgrade
        ;;

    watch-db-upgrade)
        set +e
        while true ; do
            sleep 1
            $SD42_CLI conditional-db-upgrade
        done
        ;;


    ci)
        if [ "$GIT_CRYPT_BASE64" != "" ] ; then
            echo Unlocking git crypt
            base64 -d < $GIT_CRYPT_BASE64 > ~/git-crypt-unlock.key
            git crypt unlock ~/git-crypt-unlock.key
        fi

        $SD42_CLI build
        $SD42_CLI test

        if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ] ; then
             # Committing to master, deploy!

            echo Setting ssh private key
            mkdir -p ~/.ssh
            chmod 700 ~/.ssh
            echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
            chmod 0400 ~/.ssh/id_rsa

            $SD42_CLI deploy
        fi
        ;;

    build)
        $SD42_CLI conditional-poetry-install
        $SD42_CLI conditional-npm-install
        $SD42_CLI conditional-db-upgrade
        if [ "CI_COMMIT_REF_PROTECTED" = "true" ] ; then
            $SD42_CLI build-curriculum
        else
            $SD42_CLI build-curriculum --ignore-restricted
        fi
        sass css/style.scss lms42/static/style.css
        ;;

    test)
        python -mpytest --ignore=curriculum --ignore=data "$@"
        ;;

    deploy)
        exec rsync -e 'ssh -o StrictHostKeyChecking=no' -a --info=symsafe,stats,backup,del,copy --delete-during --exclude config.sh --exclude .npm --exclude /.git --exclude __pycache__ --exclude node_modules --exclude .venv --exclude /data . $DEPLOY_TARGET
        ;;

    pg)
		poetry run postgresqlite
	    ;;


    *)
        echo "Usage: $SD42_CLI COMMAND"
        echo ""
        echo "Available COMMANDs:"
        echo "  dev          Start a development server (npm, poetry, flask and continuous curriculum build + SASS build)."
        echo "  prod         Start a production server (flask and continous npm + poetry + migrations)."
        echo "  db-revision  Autogenerate a new migration revision."
        echo "  db-upgrade   Upgrade database to the latest revision."
        echo "  shell        Create a new shell with all environment variables set correctly."
        echo "  test         Run the tests from 'tests/'."
        echo "  pg           Start the PostgreSQL command line client."
        ;;
esac
