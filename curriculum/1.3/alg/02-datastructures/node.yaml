name: Data structures
goals:
    datastructures: 1
intro:
    Get comfortable...:
        - 
            link: https://www.youtube.com/watch?v=a_agPzERnKI
            title: Python Data Structures Tutorial
            info: This is a *long* video! We recommend you watch the first 54 minutes (stop when he starts talking about Max Heaps). It introduces you to `set`s, `tuple`s and `deque`s. Also it reviews some of the things you have learned about `list`s and `dict`s, but adds quite a few tricks that will make your life easier. *Hint:* you may want to accelerate playback for topics that you're already comfortable with.
        -
            link: https://docs.python.org/3/tutorial/datastructures.html
            title: The Python Tutorial - Data Structures
            info: In case you prefer reading over watching. This is a little less detailed than the video though.
        -
            link: https://www.youtube.com/watch?v=2wM6_PuBIxY
            title: Jack Learns the Facts About Queues and Stacks
            info: Just for fun. A nicely illustrated poem about queues (`deque`s) versus stacks (`list`s).
assignment:
    -
        text: Fill in the data structure comparison table in the provided `comparison.txt` file.
        0: 15-28 errors
        1: 7-14 errors
        2: 4-6 errors
        3: 2-3 errors
        4: 0-1 errors
    -
        text: |
            Provided are two files, one containing a list of American English words and the other one containing a list of British English words. Write a script that calculates the number of American words that the British don't recognize. 
            
            Use the right data structure(s) to primarily be as fast as possible, and secondarily use the least amount of RAM memory. Regarding the latter: you should be able to do this without loading both word lists entirely into memory.
        ^merge: feature
        2: The implementation is O(n+m) but uses a dict or first loads the data for both lists in an arrays. 
        3: The implementation is O(n+m) and uses a set, but loads both data sets into memory.
    - 
        text: |
            Write a script that outputs an *alphabetically ordered* list of words that are in *both* the American and British dictionaries. The Python list `sort` function can be used.
        
            Use the right data structure(s) to primarily be as fast as possible, and secondarily use the least amount of RAM memory. Regarding the latter: you should be able to do this without loading *both* word lists entirely into memory.
        ^merge: feature
    -
        link: https://thomas-cokelaer.info/blog/2017/12/how-to-sort-a-dictionary-by-values-in-python/
        title: How to sort a dictionary by values in Python
        info: Various ways to sort the contents of a dictionary by value. You'll probably need to do this in the next objective.
    -
        text: |
            Take the first three letters of each of the American English words, and count how often they occur. Output the 20 most popular word prefixes. Words with less than three letters can be skipped
            
            Use the right data structure(s) to primarily be as fast as possible, and secondarily use the least amount of RAM memory. Regarding the former: you only need to go over all words once. Regarding the latter: you should not need to load the word list entirely into memory.

            *Hint*: use a `dict` and just count!
        ^merge: feature
    -
        text: What are the time and space complexities O(..) of the programs you have written? Write them as comments in your programs. You can use `m` and `n` for the numbers of words in the two word lists.
        0: All wrong.
        1: Three errors by a factor of `log n`, or two larger errors.
        2: Two error by a factor of `log n`, or a single larger error.
        3: A single error by a factor of `log n`.
        4: All perfect.
