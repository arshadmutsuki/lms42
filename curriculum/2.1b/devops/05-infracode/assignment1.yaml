- Introduction: |
    Terraform is a tool that helps you create and manage your cloud infrastructure from your laptop. It uses a "declarative language" to define an infrastructure and connects to AWS to deploy that infrastructure.

- Tasks:
    - Configure access to AWS: |
        In the learning lab environment you can download a link to log in to the AWS (web)console, as you did in the previous assignment. There is also an option to connect to AWS from the command line. For this assignment you will need the credentials located at "AWS CLI" (press the "Show" button). See the image below.
        
        <img src="aws-academy-2.png" class="no-border">

        Copy the entire contents (the aws access key, secret access key and session key) in the textarea and save it in `~/.aws/credentials`. Note that you can also configure the secrets as environment variables - this is done in the tutorial - but that is not recommended. 

    - Install Terraform: |
        Install "terraform" (by HashiCorp) using the *Add/Remove Software* tool from Manjaro. 

    - Install the Visual Code Terraform extension: |
        Search for "HashiCorp Terraform"  in the market and install the extension. This plugin provides auto completion and syntax highlighting.

    - Follow the Terraform tutorial:
        -
            link: https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started
            title: Tutorial - Build Infrastructure
            info: In this tutorial, you will provision an EC2 instance on Amazon Web Services (AWS). **Skip the part where the credentials are configured as environment variables.**
        -
            link: https://learn.hashicorp.com/tutorials/terraform/aws-change?in=terraform/aws-get-started
            title: Tutorial - Change Infrastructure
            info: In this tutorial, you will modify that resource, and learn how to apply changes to your Terraform projects.
        -
            link: https://learn.hashicorp.com/tutorials/terraform/aws-destroy?in=terraform/aws-get-started
            title: Tutorial - Destroy Infrastructure
            info: In this tutorial, you will use Terraform to destroy this infrastructure.
        - |
            In order to get a basic understanding of how Terraform works you should follow the following tutorials that explain how you can create, update and remove an EC2 instance using Terraform. Successfully executing this tutorial also means that you have set up the AWS credentials correctly.

- Assignment:
    -  Create a VPC with one public subnet:
        -
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
            title: "Terraform documentation - Resource: aws_vpc" 
            info: Terraform documentation on VPC
        -
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
            title: "Terraform documentation - Resource: aws_subnet"
            info: Terraform documentation on Subnets
        -
            text: |
                Create an terraform configuration for the following infrastructure.

                ![VPC with one subnet](devops-terraform-1.png)

                In the provided template you will find `main.tf` where you should put your code. After you have successfully deployed your infrastructure you should open the AWS console and validate whether the VPC and subnet have been created. Do this at the end of every objective. 

            ^merge: feature            
            code: 0

    -  Set up the routing:
        - 
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
            title: "Terraform documentation - Resource: aws_internet_gateway"
            info: Terraform documentation on the Internet Gateway
        - 
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
            title: "Terraform documentation - Resource: aws_route_table"
            info: Terraform documentation on the route table.
        - 
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route
            title: "Terraform documentation - Resource: aws_route"
            info: Terraform documentation on the route.
        - 
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
            title: "Terraform documentation - Resource: aws_route_table_association"
            info: Terraform documentation on the route table association.
        -
            text: |
                In order to correctly set up the routing in the VPC we need to have the following:
                - An internet gateway to connect our VPC to the internet (and allow internet traffic to go in and out of our VPC).
                - A routing table with the correct route to route traffic it the VPC and through the internet gateway.

                Create an terraform configuration for the following infrastructure.

                ![VPC with one subnet](devops-terraform-1b.png)

                Extend the provided template `main.tf` so that the changes are reflected in your infrastructure.

                Note that you should also create a *route table association* after you have set up your route table and your route. The association should be set up between your subnet and your route table.

            ^merge: feature      
            code: 0

    -  Launch an EC2 instance in this subnet:
        -
            link: https://learn.hashicorp.com/tutorials/terraform/aws-variables?in=terraform/aws-get-started
            title: Tutorial - Define Input Variables
            info: Terraform configurations can include variables to make your configuration more dynamic and flexible.
        -
            link: https://learn.hashicorp.com/tutorials/terraform/functions#create-a-bash-script-with-templatefile
            title: Create a bash script with templatefile
            info: This tutorial explains how to provision an EC2 instance using a a bash script in a templatefile. 
        -
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
            title: "Terraform documentation - Resource: aws_security_group"
            info: Terraform documentation on security groups.
        -
            text: |
                Extend your terraform configuration (main.tf) so that it launches an EC2 instance in the previously created (public) subnet. You should:
                - Create an instance with an `Ubuntu Server 22.04 LTS (HVM), SSD Volume Type` AMI, listed in the AWS *AMI Catalog*. 
                - Install nginx on this instance (use a template file for your script, see provided resource). 
                - Create a security group for this instance that allows HTTP and SSH traffic to this instance.

                If successfully created the default welcome page should be shown when you navigate to the instance's ip address or DNS name.
                
                *Note*: to access your instance you should be in possession of a PEM file for a valid key pair. You are allowed to reuse a previous key pair but if you do not have one you should manually create on in the AWS console. You should not create the key pair using terraform!
            ^merge: feature
            weight: 0.5
            code: 0

    -  Extend the EC2 instance:
        -
            text: |
                Running a web server with a default welcome page is no fun. So let's update the provisioning of the instance so that it runs a simple web app.

                Your tasks:
                - Create a simple static web application. You are free to create a new simple web page or choose a previously created one.
                - Create a git public repository and push your web page code to this repository. 
                - Update your user data script so that the web page is pulled from the repository and stored in the proper directory so that it is correctly served by nginx. Note that you should set your repository to public in order to pull the sources in to the EC2 instance. 
            ^merge: feature
            code: 0

    -  Securely deploy to the EC2 instance:
        -
            link: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#create-a-deploy-token
            title: Gitlab create a deploy token
            info: Instructions on how to create a deploy token for a Gitlab project.
        -
            text: |
                In the previous objective you have created a public repository so that the code can be accessed by AWS when a new EC2 instance needs to be created. This however means that we need to make our code publicly available to everyone (which is not always desired). So let's secure our repository and safely deploy the site to our EC2 instance.

                Your tasks:
                - Make your repository private: 
                    1. Go to your Gitlab repository and then **Settings -> General**
                    2. Scroll down to **Visibility, project features, permissions** and click on "Expand"
                    3. Change your **Project visibility** to *private* and click on "Save".
                - Create a deploy token (see provided resource for the details).
                - After you have created a deploy token you should be able to clone the private repository using the following command:
                ```sh
                $ git clone https://<deploy-token>:<deploy-token-password>@<url-to-git-repo>

                -- Example: git clone https://gitlab+deploy-token-2098546:nSTvLa5DxJA7WxkGxnGL@gitlab.com/sealy/private-deploy.git
                ``` 
                - Update your user data script so that it can now securely pull your code from the private repository.
            ^merge: feature
            code: 0
            weight: 0.5


    -  Extend your VPC with a second public subnet:
        -
            link: https://jhooq.com/terraform-for-and-for-each-loop/
            title: Understanding terraform count, for_each and for loop?
            info: This article and video explain how the count, for_each and for loop in terraform work.   
        -
            text: |
                Terraform has some useful features to create infrastructures. One of these features is the *for each* meta argument. Using the for each you can easily create multiple (similar) resources. In this objective your need to create a second subnet as shown in the figure below. Update your terraform file so that the subnets are created from a variable (stored in `variables.tf`) containing a set with the two subnets. Comment out the EC2 instance you have created in the previous objective (so you can still show it when this assignment is graded). 

                ![VPC with two subnets](devops-terraform-1a.png)

                Note: Don't forget to update your route table accordingly.
            ^merge: feature
            code: 0


    -  Launch an autoscaling group in the subnets:
        -
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group
            title: "Terraform documentation - Resource: aws_autoscaling_group" 
            info: Terraform documentation on Autoscaling.
        -
            link: https://egkatzioura.com/2020/01/18/autoscaling-groups-with-terraform-on-aws-part-1/
            title: "Autoscaling Groups with terraform on AWS Part 1: Basic Steps"
            info: A practical example with explanations.
        -
            text: |
                Extend the terraform template to create an auto scaling group to launch new instances. The auto scaling group should have a minimum size of 2, a desired size of 2 and a maximum size of 4. Use the user data to clone [this](https://gitlab.com/sealy/simple-webapp) git repository and install and run the simple web app (also know from the previous assignment). As a scaling policy you should set the "Average CPU utilization". Give it a very low threshold value so that the scale out is easily triggered.

                Your final infrastructure should look like this:

                <img src="devops-terraform-2.png" class="no-border">

                Note: If you run into trouble configuring the terraform template, try to create the autoscaling group using the AWS con
            weight: 2
            ^merge: feature
            code: 0

    -  Load balance your instances:
        -
            link: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb
            title: "Terraform documentation - Resource: aws_lb" 
            info: Terraform documentation on the Load Balancer
        -
            text: |
                For the final objective you should create a load balancer to load balance the instances in the autoscaling group. Your final infrastructure should look like this:

                <img src="devops-terraform-3.png" class="no-border">
            ^merge: feature
            code: 0

    -  Destroy your infrastructure:
        -
            text: |
                Once you have a working infrastructure you should clean it up so that you do not waste resources in AWS. In order to validate that everything is working correctly you can deploy a clean version of your infrastructure afterwards. If your configuration is correct your infrastructure should be created without issues. Just be sure te remove everything again afterwards. 
            must: true
