- Introduction: |
    In this assignment you will get to know the basic building blocks for creating an infrastructure in the cloud using Amazon Web Services (AWS).

- Tasks:
    - Getting access to AWS: |
        In order to get access to a sandbox AWS environment you will need to log into AWS.
        Please ask your teacher to provide you access to the AWS console.

        **Teacher note:**
        1. Log in to [https://awsacademy.instructure.com](https://awsacademy.instructure.com)
        2. Select the course AD AWS course from the dashboard
        3. Go to the "People" section and add the new student (using his/her saxion email adres)

    - Using AWS Academy: |
        Your teacher will onboard you in the AWS Academy environment. Once you have logged in successfully you should see one course on your dashboard. Click on the course and select the "Modules" in the left menu and choose the "Learner Lab" option (see screenshot below).

        <img src="aws-academy-1.png" class="no-border">

        Once you are on the learning lab page you can start the lab using the controls on the top right of the page (see screenshot below). Once the lab has started you can click on "AWS Details" in the top right menu. You can log in to AWS (web) console by clicking on the "Download URL" button at AWS SSO. This downloads a txt file containing a link which can be used to log in to the AWS console for the lab environment. For the next assignment (infrastructure as code) you will need the credentials located at "AWS CLI" (press the "Show" button). 
        
        <img src="aws-academy-2.png" class="no-border">

        *Note:* the lab will be **active for a duration of 4 hours**. After that it will be shutdown automatically. This means that any running instance will be shutdown and when you start the lab again you will have to spin them up again.

- Assignment:
    - Carve out your own piece of cloud:
        -
            link: https://docs.aws.amazon.com/vpc/latest/userguide/how-it-works.html
            title: How Amazon VPC works
            info: The official AWS documentation explaining what a VPC is.
        - 
            link: https://docs.aws.amazon.com/vpc/latest/userguide/working-with-vpcs.html
            title: Working with VPCs and subnets
            info: The following procedures are for manually creating a VPC and subnets. You also have to manually add gateways and routing tables. 
        -
            text: |
                In the image below a VPC with two subnets (each in one availability zone) is shown. Your task is to create a virtual private cloud (VPC) for this infrastructure. You should:
                - create a VPC using the "VPC Only" option (instead of the "VPC and more" which creates other things as well).
                - create the two subnets in your VPC and in different availability zones (use the IP ranges described in the image).
                
                <img src="devops-cloud-1.png" class="no-border">

                *Note*: (as good practice) you should provide a meaningful tag to all your resources.  
            must: true

    - Set up networking:
        -
            link: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html
            title: Configure route tables
            info: Explains how routing within AWS works and how you can configure it.
        - 
            text: |
                At this point you have created "placeholders" (the subnets) in which instances can be launched. But for these instances to be reachable through the internet we need to set up the routing of traffic for, to and within our VPC.

                Your task is to:
                - create an internet gateway for your VPC (which allows communication between the VPC and the internet),
                - create a main route table for your VPC (which directs the traffic within the VPC),
                - create a route within the route table so that all traffic (except local traffic) is routed to the internet gateway (this means that the destination for the route is `0.0.0.0/0`, which represents all IPv4 addresses should be sent to the internet gateway (the target) that's attached to your VPC). 
                
                *Note*: When everything is configured correctly you should have **two** routes (`10.0.0.0/16` with target 'local' and `0.0.0.0/0` with your internet gateway as target) 
            must: true

    - Launch your first instance:
        -
            link: https://youtu.be/nA3yN76cNxo
            title: Amazon EC2 Security Groups Tutorial
            info: Explains how security groups work within AWS and how you can configure inbound and outbound traffic using them.
        - 
            text: |
                Now it is time to launch an EC2 instance in one of the two subnets. 

                Your task is to:
                - launch an instance based on Ubuntu 20.04 image and of type `t2.micro`,
                - create a security group that allows HTTP (port 80) and SSH (port 22) traffic to your instance.
                
                *Note*: (as good practice) you should give your instance a "*Name*" tag so you can find it later on in your list of instances.

                *Note*: Do not lose your keypair! You need it to access you EC2 instance.

                <img src="devops-cloud-2.png" class="no-border">

                To validate if you have successfully created your EC2 instances you should:
                - open an SSH connection to the instance and install nginx on it (something with `apt install`),
                - validate whether nginx is installed correctly by navigating to the public ip of the instance in your browser and see the nginx welcome page.
            
                *Note*: If you do not see the nginx welcome page this could be because it is not correctly installed or because the security group does not allow HTTP traffic to instance.

            must: true

    - Create high available instances:
        -
            text: |
                The subnets in your VPC are both created in separate availability zones. This means that they are created in data centers located in different regions of the world. If one of these data centers fails then the resources in the data centers of the other availability zones are still available. This makes it possible to create high available services which are services that run on multiple servers and continue running if one server stops.

                As a first step we will create instances in both zones (like in the figure below) and in the next objective we will create a load balancer to distribute traffic to these instances. 

                <img src="devops-cloud-3.png" class="no-border">

                Your task is to:
                - log into the instance you have created in the previous objective (using SSH). Clone the [https://gitlab.com/sealy/simple-webapp](https://gitlab.com/sealy/simple-webapp) git repository to this instance. This (simple) web application displays the host name and the current date and time of the instance. To run the app you should first build it (by running `npm install` in the terminal) and the start it (using `npm run start`). *Note* that you should install *npm* first on the instance. 
                - create a new  security group for this instance to allow traffic over port 3000. The existing security group should remain the same.
                - validate that the simple web application is correctly running on the instance by navigating to the instance public ip and port 3000 in the browser (the address should look something like `http://<ip>:3000`). If installed correctly you should see the instance IP address displayed on the page.
                - create a second instance (also an  Ubuntu 20.04 image) in the other subnet, connect to this instance, install and run the simple web application on it.
                - validate that the second instance is also running correctly over port 3000 via you browser.

                *Note*: The `npm run start` command runs the instance in the foreground. This means that pressing `control + C` (or logging out of the instance) will stop the app from running. You can run the process in the background using `&`.
            must: true

    - Load balance the instances:
        -
            link: https://youtu.be/E-RYaMn348g
            title: AWS EC2 Elastic Load Balancing Introduction
            info: Explains how load balancing in AWS works.
        -
            link: https://youtu.be/OGEZn50iUtE
            title: AWS Elastic Load Balancing Tutorial
            info: This step-by-step tutorial explains how to create and configure a load balancer in AWS. 
        -
            text: |
                In this objective we are going to load balance the HTTP traffic to both instances. Currently you have to know the IP address (or DNS name) of the instance in order to reach it. With a load balancer you can route traffic to one of the two instances using a single DNS name (in other words via one single URL). 
                
                Your task is to:
                - create a load balancer that receives (HTTP) requests on port 80 and forwards them to the instances on port 3000,
                - create a target group with port 3000 in which the instances can be placed.
                
                The load balancer works when the web application on one of the instances is shown when you navigate to the load balancers DNS in your browser. Refreshing the page should (sometimes) display the page on the other instance. 

            must: true

    - Autoscale your instances:
        - 
            link: https://youtu.be/jvMoWjsP7Pk
            title: AWS Auto Scaling Groups Introduction
            info: Explains how Auto Scaling Groups in AWS work.
        -
            link: https://youtu.be/NNrDr8cnUzs
            title: Auto Scaling Groups Tutorial
            info: This step-by-step tutorial explains how to create and configure auto scaling in AWS.
        -
            text: |
                To complete our high availability infrastructure we will use an autoscaling group to add more instances when the load on the servers becomes too high (and remove instances when te load is sufficiently low). 

                Before you set up the auto scaling you should:
                - stop the instances currently running the simple web application. Don't terminate them just yet.
                - remove the instances from the load balancers target group.

                After you have cleaned up your target group we are going to set up our final infrastructure as shown below:

                <img src="devops-cloud-4.png" class="no-border">

                To achieve this you should:
                - create an auto scaling group to launch new instances. The auto scaling group should have a minimum size of 2, a desired size of 2 and a maximum size of 4. 
                - create a launch template that provisions a new instance. In the "*user data*" field you should clone the git repository and install and run the simple web app. Don't forget to install npm on the instance first ans run the application in the background.
                -  set the scaling policy to  "Application Load Balancer Request Count Per Target". Give it a very low threshold value so that the scale out is easily triggered.

                *Note*: You can see how the user data was executed by looking in the log file on the instance. Log in to the instance (using SSH) en type `cat /var/log/cloud-init-output.log` to see contents of the log file (the log contains many messages but the last part of the log is the most relevant).

                To validate whether the auto scaling works you should navigate to the DNS name of the load balancer in your browser. You should refresh the page a couple of times and check whether new instances are created. Note that this process can take a while (about 3 minutes) and it would be wise to use and auto refresh plugin for your browser (for example Tab Auto Refresh for Firefox). 
            must: true
            # 0: Not working
            # 2: Somewhat working
            # 4: Perfect

    - Securing the load balancer:
        -
            text: |
                If we have a working load balancer set up we want to secure it by only allowing access to the instances via the load balancer. 
                
                Your task is to:
                - change the security group for the instances of the target group (for port 3000) so that it is dependant on the security group of the load balancer

                Validate that you can not access the instances directly on port 3000 but that you can access the web application via the load balancer.            
            must: true


    - Serve static website using S3:
        -
            link: https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/module-1/
            title: Module 1. Static Web Hosting
            info: A step-by-step tutorial on how to host a static web page on AWS using S3.
        -
            text: |
                The Amazon Simple Storage Service (S3) is an object storage which can be used for many purposes like storing log files or media files and also static web content. S3 is designed to provide 99.999999999% durability and 99.99% availability of objects over a given year which means data is almost never lost.

                For this objective you will create an S3 bucket and configure it to serve a static web application. The application you will need to put on S3 is a simple todo app found here: [https://gitlab.com/sealy/simple-todo-app](https://gitlab.com/sealy/simple-todo-app). 
                
                Your task is to:
                - create an S3 bucket,
                - clone the git repository to your laptop and build it,
                - copy the result of the build (look in the `dist` folder) to the S3 bucket,
                - configure the bucket to serve the content as a static web page.

            must: true

    - Wrapping up:
        text: | 
            Because you are running the instances in a sandbox environment they will be stopped after a certain period of time. When you have completed this assignment you can stop the instances by setting the minimum and desired size of your auto scaling group to 0. When discussing your work with the teacher set the minimum and desired size back to 2 so that the proper working of your infrastructure can be verified.
        must: true