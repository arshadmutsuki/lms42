======================================== Tokens ========================================
'var' <keyword>
'max' <identifier>
'=' <operator>
'input_num' <identifier>
'(' <delimiter>
'"Type a positive integer.. "' <string>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'i' <identifier>
'=' <operator>
'1' <number>
';' <delimiter>
'while' <keyword>
'(' <delimiter>
'i' <identifier>
'<=' <operator>
'max' <identifier>
')' <delimiter>
'{' <delimiter>
'if' <keyword>
'(' <delimiter>
'(' <delimiter>
'i' <identifier>
'%' <operator>
'15' <number>
')' <delimiter>
'==' <operator>
'0' <number>
')' <delimiter>
'print' <identifier>
'(' <delimiter>
'"fizzbuzz"' <string>
')' <delimiter>
';' <delimiter>
'else' <keyword>
'if' <keyword>
'(' <delimiter>
'(' <delimiter>
'i' <identifier>
'%' <operator>
'5' <number>
')' <delimiter>
'==' <operator>
'0' <number>
')' <delimiter>
'print' <identifier>
'(' <delimiter>
'"buzz"' <string>
')' <delimiter>
';' <delimiter>
'else' <keyword>
'if' <keyword>
'(' <delimiter>
'(' <delimiter>
'i' <identifier>
'%' <operator>
'3' <number>
')' <delimiter>
'==' <operator>
'0' <number>
')' <delimiter>
'print' <identifier>
'(' <delimiter>
'"fizz"' <string>
')' <delimiter>
';' <delimiter>
'else' <keyword>
'print' <identifier>
'(' <delimiter>
'i' <identifier>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'i' <identifier>
'=' <operator>
'i' <identifier>
'+' <operator>
'1' <number>
';' <delimiter>
'}' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.Declaration(
                identifier='max',
                expression=tree.FunctionCall(
                    name='input_num',
                    arguments=[
                        tree.Literal(value='Type a positive integer.. ')
                    ]
                )
            ),
            tree.Declaration(identifier='i', expression=tree.Literal(value=1)),
            tree.WhileStatement(
                condition=tree.BinaryOperator(
                    left=tree.VariableReference(name='i'),
                    operator='<=',
                    right=tree.VariableReference(name='max')
                ),
                block=tree.Block(
                    statements=[
                        tree.IfStatement(
                            condition=tree.BinaryOperator(
                                left=tree.BinaryOperator(
                                    left=tree.VariableReference(name='i'),
                                    operator='%',
                                    right=tree.Literal(value=15)
                                ),
                                operator='==',
                                right=tree.Literal(value=0)
                            ),
                            yes=tree.Block(
                                statements=[
                                    tree.FunctionCall(
                                        name='print',
                                        arguments=[
                                            tree.Literal(value='fizzbuzz')
                                        ]
                                    )
                                ]
                            ),
                            no=tree.IfStatement(
                                condition=tree.BinaryOperator(
                                    left=tree.BinaryOperator(
                                        left=tree.VariableReference(name='i'),
                                        operator='%',
                                        right=tree.Literal(value=5)
                                    ),
                                    operator='==',
                                    right=tree.Literal(value=0)
                                ),
                                yes=tree.Block(
                                    statements=[
                                        tree.FunctionCall(
                                            name='print',
                                            arguments=[
                                                tree.Literal(value='buzz')
                                            ]
                                        )
                                    ]
                                ),
                                no=tree.IfStatement(
                                    condition=tree.BinaryOperator(
                                        left=tree.BinaryOperator(
                                            left=tree.VariableReference(
                                                name='i'
                                            ),
                                            operator='%',
                                            right=tree.Literal(value=3)
                                        ),
                                        operator='==',
                                        right=tree.Literal(value=0)
                                    ),
                                    yes=tree.Block(
                                        statements=[
                                            tree.FunctionCall(
                                                name='print',
                                                arguments=[
                                                    tree.Literal(value='fizz')
                                                ]
                                            )
                                        ]
                                    ),
                                    no=tree.Block(
                                        statements=[
                                            tree.FunctionCall(
                                                name='print',
                                                arguments=[
                                                    tree.VariableReference(
                                                        name='i'
                                                    )
                                                ]
                                            )
                                        ]
                                    )
                                )
                            )
                        ),
                        tree.Declaration(
                            identifier='i',
                            expression=tree.BinaryOperator(
                                left=tree.VariableReference(name='i'),
                                operator='+',
                                right=tree.Literal(value=1)
                            )
                        )
                    ]
                )
            )
        ]
    )
)

======================================== Running program ========================================
Type a positive integer.. 1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz
fizz
22
23
fizz
buzz
26
fizz
28
29
fizzbuzz
31
32
fizz
34
buzz
fizz
37
38
fizz
buzz
41
fizz
