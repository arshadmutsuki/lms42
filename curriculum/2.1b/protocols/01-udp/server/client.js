const PAYLOAD_SIZE = 500;

const dgram = require('dgram');
const BufferBuilder = require('buffer-builder');
const BufferReader = require('buffer-reader');
const fs = require('fs');


function Download(ip, file) {
	this.ip = ip;
	this.file = file;
	this.writeFd = fs.openSync(file, 'wx');
	
	this.socket = dgram.createSocket('udp4');
	this.socket.on('message', this.onMessage.bind(this));
		
	this.transferId = (0|Math.random()*65536) + (0|Math.random()*65536)*65536;
	this.receivedFrames = [];
	this.sendRequest();
}

Download.prototype.resetProgressTimeout = function() {
	if (this.progressTimeout) {
		clearTimeout(this.progressTimeout);
	}
	this.progressTimeout = 0;
	if (!this.ready) {
		this.progressTimeout = setTimeout(this.sendRequest.bind(this), 2500);
	}
};

Download.prototype.sendRequest = function() {
	var bb = new BufferBuilder();
	bb.appendString("SaxTP");
	bb.appendUInt8(0);
	bb.appendUInt32BE(this.transferId);
	bb.appendString(this.file);
	this.socket.send(bb.get(), 29588, this.ip);

	this.resetProgressTimeout();
};

Download.prototype.onMessage = function(buf, rinfo) {
	const br = new BufferReader(buf);

	if (!br.nextBuffer(5).equals(Buffer.from("SaxTP"))) {
		console.error("invalid packet received", buf, rinfo);
		return;
	}
	const type = br.nextUInt8();
	const transferId = br.nextUInt32BE();

	if (type!=128) {
		console.error("invalid packet type received", type, buf, rinfo);
		return;
	}

	// ReponseData packet
	const seq = br.nextUInt32BE();

	this.resetProgressTimeout();

	// send an ResponseAck
	var bb = new BufferBuilder();
	bb.appendString("SaxTP");
	bb.appendUInt8(1);
	bb.appendUInt32BE(this.transferId);
	bb.appendUInt32BE(seq);
	this.socket.send(bb.get(), rinfo.port, rinfo.address);

	// Save the frame
	const frame = this.receivedFrames[seq] = br.nextAll();
	if (frame.length < PAYLOAD_SIZE) this.eof = true;

	if (!this.eof || this.ready) return; // the end-of-file frame has not been received yet

	// check if all prior frames have been received as well
	for (var i = 0; i < this.receivedFrames.length; i++) {
		if (this.receivedFrames[i] === undefined) {
			return; // frame still missing
		}
	}

	// received all frames!
	this.ready = true;
	this.resetProgressTimeout(); // this stops the timeout from firing again

	if (this.receivedFrames.length===1 && this.receivedFrames[0].length===0) {
		// empty file received
		console.error("no such remote file: "+this.file);
		fs.unlinkSync(this.file);
	}
	else {
		// write all frames to the output file
		for (var i = 0; i < this.receivedFrames.length; i++) {
			fs.writeSync(this.writeFd, this.receivedFrames[i]);
		}
	}
	fs.closeSync(this.writeFd);

	// delay closing the socket for 50ms, to allow sending of the last ack(s)
	setTimeout(() => {
		this.socket.close();
	}, 50);
};



let ip = process.argv[2];
let file = process.argv[3];
if (!ip || !file) {
	console.log("syntax: ./saxtp-client.js <ip> <file>");
	return;
}
new Download(ip, file);

