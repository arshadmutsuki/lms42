name: Mobile, desktop, tablet, wearable
description: We'll explore how different devices sometimes require different designs.
goals:
    device-design: 1
days: 2
todo: Add a watch-template to WireText. Make it more clear what kind of smartwatch functionality we have in mind.
resources:
  - 
    link: https://uxcam.com/blog/mobile-ux/
    title: The Complete Guide to Understanding App UX Design 2022
    info: A basic introduction of how user interfaces on desktop differ from mobile.

assignment:
  - Introduction: |
      Nowadays we have many (general purpose) devices we use everyday. The most used devices are of course the laptop and smart phone but smartwatches and tablets are often used as well. Modern application take these different screen sizes and their specific features (swiping on mobile) into account. In this assignment you will have to do so too.

      First watch the video provided in the resources to understand the difference between designing for mobile. Next watch the video below containing an application request of our customer. Finally make wireframes for the different devices as stated in the objective below.

      Good luck!  

      - [Application Request: Plant exchange](https://video.saxion.nl/media/Plant%20exchange/1_u05tspeb)

  - Take notes:
    -
      must: true
      text: |
          Write down your notes in `notes.txt`.

  - Design a desktop application:
    - 
      text: | 
        For this objective you are asked to create **low fidelity** wireframes for a **desktop** application. Your design should show which visual elements are found in your application screens and how the user can interact with these elements.

        Your tasks:
        - Create a wireframe for every screen in your application.
        - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application. For example a a "Save" button in your application will save information in an applications database.You should describe these actions. Although this "Save" example is straightforward some actions a not (for example a push notification that is sent to a different user when a message is created in the application).

      0: Wireframes do not give a clear impression of the application to be build
      4: Clear wireframes including intuitive navigation between pages.
      map:
        device-design: 1
        

  - Design a mobile application:
    - 
      text: |
        For this objective you are asked to create **low fidelity** wireframes for a **mobile** application. Your design should take into account that it runs on a mobile device.

        Your tasks:
        - Create a wireframe for every screen in your application.
        - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application.
            3. Be explicit about the use of mobile interactions, like swiping of double tapping (*write this down in the note to the screen*).


      0: Wireframes do not give a clear impression of the application to be build
      4: Clear wireframes including intuitive navigation between pages.
      map:
        device-design: 1


  - Design a useful smartwatch extension:
    -
      text: |
        An interesting device to design for is the smartwatch. The interface is very small so there is little room to design your interactions. It is also not feasible to design an entire application for the smartwatch. However many application contain specific tasks that can be intuitively be implemented for a smartwatch (think of payments with your smartwatch).

        Your task:
        - Analyze the tasks in your application if they can be done using a smartwatch. 
        - Choose a task (or if you can think of a different task for your application) for which you can create a design for.
        - Create a wireframe for every "screen" on the smartwatch.
        - Explain what the interactions are and what the effect of the interaction is on the application. 

      0: No extension described
      2: Extension is unclear or not very useful or well thought out  
      4: Clear and useful extension.
      map:
        device-design: 1
