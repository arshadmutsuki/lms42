name: Class diagrams
days: 1
goals:
    class-diagram: 1
intro: 
- Class diagrams: |
    *Class diagrams* are a type of diagram that may remind you of *ERD*. But whereas *ERD*s model relation databases, *class diagrams* model object-oriented programs. We will show the most important elements below, together with corresponding Python source code.

    ## Classes

    Below is the class diagram for a single class that has two attributes (instance variables) and two methods (including the constructor).

    ```plantuml
    class Shape {
        x: int
        y: int
        __init__(x: int, y: int)
        draw()
    }
    ```

    And here is the corresponding Python:

    ```python
    class Shape:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def draw(self):
            pass # Implementation goes here
    ```


    ## Visibility

    Although the Python language does not make any distinction, your attributes and methods can generally be divided into those that should be accessible only from within the class itself, which we call *private*, and those that should be accessible from the outside, which we call *public*. 

    Generally, it is a good idea to make *all* attributes (instance variables) private. For this and all further OOP lessons, we expect you to adhere to this rule.

    Here is an example with two private attributes, two public methods and a private method:

    ```plantuml
    class Shape {
        -x: int
        -y: int
        +__init__(x: int, y: int)
        +draw()
        -draw_part()
    }
    ```

    In Python, the convention (something that many programmers agree to do, but that is not enforced by the programming language) is to prefix private methods and variables with one or two underscores. The use of two underscores will automatically prefix the name of the class to the name, so `__test` becomes `_MyClassName__test`. This prevents accidental use of private methods and attributes. Therefore, please stick to the double underscore convention for the duration of this module.

    (Note than when names are *surrounded* by double underscores, like `__init__` and `__str__`, that does *not* mean they are private, but that they have some special meaning to Python.)

    ```python
    class Shape:
        def __init__(self, x, y):
            self.__x = x
            self.__y = y

        def draw(self):
            self.__draw_part()
            self.__draw_part()

        def __draw_part(self):
            pass # Implementation goes here
    ```

    Class diagrams can also have *protected* and *package private* visibilities, but these don't make a lot of sense when working with Python, so we'll ignore them.
    

    ## Level of detail

    In the above examples, we have included all details about the classes we were modelling. Class diagrams can also be used to create a helicopter-view of your system, by leaving out details such as private attributes and methods, method arguments, types and sometimes even the public methods (and attributes).

    For example, a high-level version of the previous diagram might look like this:

    ```plantuml
    @startuml
    class Shape {
        +draw()
    }
    ```

    Or even:

    ```plantuml
    @startuml
    class Shape {
    }
    ```


    ## Inheritance

    Here's a high-level class diagram demonstrating inheritance:

    ```plantuml
    @startuml
    Shape <|-- Circle
    Shape <|-- Polygon
    Polygon <|-- Triangle
    Polygon <|-- Rectangle
    Rectangle <|-- Square

    ```

    In Python, these would just be classes using `extends`.


    ## Associations

    Besides *inheritance*, the other way that a class can relate to other classes is to have instances hold references to instances of other classes as instance variables. We call these *associations*. Associations are modelled using a line between two classes, and may have a label that clarifies the meaning of the relation.

    ```plantuml
    Man - Woman: spouse
    ```

    Classes can also have associations with themselves:

    ```plantuml
    Human - Human: spouse
    ```

    In many cases, the label describes the association from the perspective of one of the objects. In that case it can have an arrow (to clarify that the `Employee` does *not* employ the `Boss`):

    ```plantuml
    class Boss
    Boss - Employee: employs >
    ```

    All of these relations are bidirectional, meaning that both objects hold a references to the other object as an instance variable. One way, but far from the only way, to do this in Python is:

    ```python
    class Human:
        def __init__(self):
            self.__spouse = None
        def marry(spouse):
            """Set a two-way spouse association."""
            self.__spouse = spouse
            spouse.__spouse = self
    ```

    Note that, like other instance variables, references to other classes should generally be considered *private* and thus prefixed with an underscore in Python.


    ## Directional associations

    If just one object needs to refer to another object (but not the other way around), the direction of an association can be indicated by an arrowhead pointing at the class that is being referred to. 

    ```plantuml
    class User
    User -> Color: favorite
    ```

    In this case, the label doesn't need an arrow, as the perspective for it is already clear.

    ```python
    class Color:
        pass # Implementation goes here

    class User:
        def __init__(self):
            self.__favorite_color = None
        
        def set_favorite_color(color):
            self.__favorite_color = color
    ```


    ## Aggregation and composition

    Class diagrams distinguish two special types of associations:

    - **Aggregation.** One object (indicated by an outlined diamond shape) *has* another object. For example, a `Team` may *have* a coach that is a `Person`.
      ```plantuml
      class Team
      Team o-> Person: coach
      ```
    - **Composition.** One object (indicated by a solid diamond shape) *consists* partly of another object. For example, a `Coordinate` may be part of a `Shape`. The `Shape` object is the only object that uses that particular `Coordinate` instance. The `Shape` is the exclusive *owner* of the `Coordinate`. When the `Shape` ceases to exist, there is no need for the `Coordinate` to live on.
      ```plantuml
      class Shape
      Shape *-> Coordinate: position
      ```

    Aggregations and compositions can also be non-directional associations.

    In Python all types of associations result in similar code, except that in case of composition the containing object is likely to create the contained object its constructor, while in other cases it may be passed in from somewhere else in your code. This is not a hard rule though.

    ```python
    # Directional aggregation
    class Person:
        pass # Implementation goes here

    class Team:
        def __init__(self, coach: Person):
            self.__coach = coach

    # Directional composition
    class Coordinate():
        pass # Implementation goes here

    class Shape:
        def __init__(self):
            self.__position = Coordinate()
    ```


    ## Cardinality

    Associations have a *cardinality* (also known as *multiplicity*), meaning the number of instances that the relationship refers to. By default, the cardinality is assumed to be one-on-one, but it can be set by adding a number or a range of numbers near the class that is being referred to.

    In the example below, a `Person` has zero or one team, while `Each` team may have one or more members.

    ```plantuml
    Team -> "1..*" Person: members
    class Team {
        +add_member()
    }

    ```

    ```python
    class Person:
        pass

    class Team:
        def __init__(self):
            self.__members = set()

        def add_member(self, member: Person):
            self.__members.add(member)
    ```


    ## Notes
    Class diagrams may include notes, in order to clarify intent.

    ```plantuml
    class MyThing {
        +my_method()
    }

    note right {
        This is a note for the //MyThing// class.
    }
    ```

    Remember that class diagrams are not complete system specification, as they don't describe any behavior of the methods. They are just design tools that can help express an idea at an appropriate level of detail.

-
    link: https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-class-diagram/
    title: Visual Paradigm - What is a class diagram?
    info: A good, albeit non-Python-specific, introduction to class diagrams. Refer to this if something in the above doesn't make sense.
