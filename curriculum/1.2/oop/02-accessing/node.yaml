name: Attribute access
description: Private attributes and uniform access.
days: 1
goals:
    classes: 1
resources:
-
    link: https://www.pythontutorial.net/python-oop/python-private-attributes/
    title: Introduction to encapsulation in Python
    info: "An important concept of object-oriented programming is encapsulation: hiding information that is internal to your class for the outside world. Doing this is a powerful to keep large and complex applications manageable. This article explains the basics."
assignment:
-
    Let's extend the role playing game from the previous lesson, to add some more features and to improve upon its object-oriented bragging rights!

    Start by copying your solution from the previous lesson.

-
    ^merge: feature
    title: This is PRIVATE PROPERTY, get lost!
    text: |
        Modify your code to convert all class attributes to *private* attributes, meaning that you need to prefix them with two underscore and that you make sure they are only accessed by methods of the class itself. In order to interact with other classes, you may no longer touch their attributes. Instead, you should ask the other class nicely to do what you want it to do, by calling a method on it.

        Hint: you may want to create an additional method like `ask_attack_target(self, attacking_player)` in your `Game` class.


- |
    <div class="notification is-important">
        From this point onwards, we expect you to use only private attributes (with two underscores) in your classes in all of your OOP assignments! This is not something that is commonly done in Python code you'll find 'in the wild', but it will help you learn *abstraction* in code, and when done right it sure helps readability.
    </div>

-
    ^merge: feature
    title: Let there be weapons
    text: |
        Create a class named `Weapon` with the following methods:

        - A constructor: Should create attributes for the weapon's name, strength, reliability and weight, the values for which are semi-randomly provided by this code:

            ```python
            # [(noun, strength, reliability, weight,)...]
            NOUNS = [('sword', 4, 8, 5), ('shield', 1, 8, 8), ('bow', 8, 2, 3), ('canon', 9, 2, 15), ('force field', 2, 7, 1), ('drone', 9, 2, 20), ('tank', 9, 7, 45)]
            # [(adjective, multiplier,)...]
            ADJECTIVES = [('wooden', 1), ('bronze', 2), ('steel', 3), ('diamond', 4), ('nanofiber', 5), ('unobtainium', 6), ('epic', 7), ('legendary', 8), ('eternal', 9)]

            noun, strength, reliability, weight = random.choice(NOUNS)
            adjective, multiplier = random.choice(ADJECTIVES)
            strength = round(strength * multiplier / 5) # Apply the multiplier to strength
            name = f"{adjective} {noun}".title()
            ```
        - `get_damage`: Every time this is called, it decrements the weapon's reliability by 1 (due to wear and tear) and returns the amount of damage the weapon now does to an opponent. This is calculated by multiplying the strength with a random integer between 0 and the weapon's reliability.
        - `__str__`: Should return something like: *Bronze Bow (12 strength, 5 reliability, 4 kg)*. When the reliability is 0, the return value should be similar to: *Bronze Bow (BROKEN)*.
        - `upgrade`: Increases either the strength *or* the reliability of the weapon with some random amount [1..15]. It also increases the weapon's weight with a random amount [1..5]. Afterwards, it prints the stats for the upgraded weapon (relying on `__str__`).

-
    ^merge: feature
    title: Extend the Player class
    weight: 2
    text: |
        - When it's a players turn, he/she should get two additional options:
            - *Find a weapon*. This should instantiate a new `Weapon` and add it to a (private) list of weapons that the object should have as an attribute.
            - *Upgrade a weapon*. This should allow the user to select (using `ask_choice`) one of his/her weapons, for which the `upgrade` method must be called.
        - The `attack` method should be modified, such that it asks the user to select a weapon used for the attack. The damage to the selected opponent should be determined by the weapon's `get_damage` method.
        - Two new methods should be implemented:
            - `get_weapon_count`, which should return the number of weapons the player is holding.
            - `get_weapon_weight`, which should return the total weight for all weapons the player is holding.
        - The `__str__` method should be updated to return something like this: *Peter (100 hp, 3 weapons, 44 kg)*, making use of the above two methods.
        - By the end of each turn, the `play_turn` method should check if the player's weapon weight doesn't exceed 50 kg, as the player cannot carry more than that. The player is asked to select weapons to drop (remove from the list of weapons) until enough weight was lost.

-
    link: https://www.youtube.com/watch?v=XzkhtWYYojg
    title: Using the Python pickle Module
    info: As a developer, you may sometimes need to send complex object hierarchies over a network or save the internal state of your objects to a disk or database for later use. To accomplish this, you can use a process called serialization, which is fully supported by the standard library thanks to the Python pickle module.
-
    link: https://www.geeksforgeeks.org/understanding-python-pickling-example/
    title: Understanding Python Pickling with example
    info: Python pickle module is used for serializing and de-serializing a Python object structure. Any object in Python can be pickled so that it can be saved on disk. *Note:* in one of the examples a file is opened with flags `'ab'` (append binary). This should be `'wb'` (write binary).

-
    ^merge: feature
    title: Save/load game
    weight: 1
    text: |
        We want to be able to automatically save the game after each turn, and resume it when we next start the game.

        - After each turn, have `Game.run` use `pickle` to save the state of the entire `Game` object to a file.
        - Instead of just creating a `Game()` on startup, the application should see if the save file exists, and if it does use `pickle` to restore the `Game` object. Note that the constructor won't be called in this case - it will just set all attributes to how they were. This includes restoring any other objects (indirectly) referred to be these attributes.
        - Note that the `turn` variable is *not* preserved. It *should* be though, in order for the correct player to play first. Make this happen!
