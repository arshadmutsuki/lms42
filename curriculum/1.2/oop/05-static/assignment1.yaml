Tic Tac Toe Pro: |
    You've built Tic Tac Toe before. Let's do it again, only this time we'll be adding quite a few features:

    - Choice between a simple and a fancy (multi-colored) board.
    - Two types of player bots to choose from, besides human players.
    - Configurable board size.

    The end result should work something like this:

    <link rel="stylesheet" type="text/css" href="/static/asciinema-player.css" />
    <script src="/static/asciinema-player.min.js"></script>

    <script>
        let el = document.createElement('div');
        document.currentScript.after(el);
        AsciinemaPlayer.create('ttt-demo.cast', el, {loop: true, poster: 'npt:0:10'});
    </script>

    Try to imagine what (even worse) mess your code would become if you were to add all of these features to your original, non-object-oriented implementation. 😨

    OOP to the rescue!

    Your final program at the end of this assignment should have a Class diagram similar to the following diagram:

    ```plantuml
    @startuml

    class TicTacToe {
        +play()
        -{static} create_player(): Player
    }
    TicTacToe o--> Board
    TicTacToe o--> "2" Player

    abstract class Board {
        -size: int
        +Board(size: int)
        +get_size(): int
        +set_marker(square: int, marker: str)
        +get_marker(square: int): str|None
        +get_winner_marker(): str|None
        +check_draw(): bool
        {abstract} +print_board()
        {abstract} +ask_square(): int
    }

    class SimpleBoard extends Board {
        +print_board()
        +ask_square(): int
    }

    class FancyBoard extends Board {
        +print_board()
        +ask_square(): int
    }

    abstract class Player {
        -name: string
        -token: symbol
        {abstract} +choose_square(board: Board): int
    }

    class HumanPlayer extends Player {
        +choose_square(board: Board): int
    }
    note bottom {
        Uses ask_square().
    }

    class RandomPlayer extends Player {
        +choose_square(board: Board): int
    }

    class SequentialPlayer extends Player {
        +choose_square(board: Board): int
    }

    @enduml
    ```

Board:
    ^merge: generic
    text: |
        Create a `Board` class. It should have at least...

        - A method call for setting and getting `X`es and `O`s to/from a given square.
        - A method that prints the current state of the board, using simple monochrome output. You can use the `+` `-` and `|` characters for the borders.
        - A method that returns the winner's symbol (`X` or `O`) if there is a winner.
        - A method to check if there's a draw (the board is full).

        Create a small test program (or even better, a proper *unit test*, if you know how), to make sure that your board behaves as it should before moving on to the next objective.

The basic game:
    ^merge: generic
    text: |
        Create a basic working version of the game, by implementing a (human) `Player` and the `TicTacToe` class.

        The `Player` class should...

        - Ask the player for his/her name in the constructor. However, there's a rule: each player can only play once while your program is running. If, in a subsequent game (during the same program execution) or for the second player in the same game, the same name is entered, the program should respond with `Denied! You have already played.` and ask again. Use a *static* attribute for this.
        - Hold the player's token (`X` or `O`).
        - Have a method that gets a `Board` object as an argument, asks the user which square he/she chooses, and returns the coordinates of that square.

        The `TicTacToe` class should...

        - Ask the user for the desired board size.
        - Instantiate the board and two players.
        - Have a method that plays the game, alternating player turns until there is a winner or the board is full.

        You should now be able to play human-vs-human matches displayed in the simple monochrome format, with user-configured board sizes.

Computer players:
    ^merge: generic
    text: |
        Split up the `Player` class into two parts: an *abstract* `Player` class and a `HumanPlayer` class that inherits from the former. In addition, implement two player bots:

        - A random player, the picks random available squares. Random players should automatically pick a random name.
        - A sequential player, that always picks the first available square. Sequential players should automatically get a name containing a sequential number, so `Seq1`, `Seq2`, etc.

        Use polymorphism to have a player choose a square in the way that is appropriate for that player type.

        In the `TicTacToe` class, ask the user what player types he/she desires. To prevent code duplication and to prevent the constructor from becoming too long, you should use a `static` method for this.

Fancy board:
-
    link: https://blessed.readthedocs.io/en/latest/intro.html
    title: Blessed documentation - intro
    info: Blessed is a library for creating terminal based user-interfaces. It helps you to position the cursor, use colors, handle input, and many other things.
-
    link: https://blessed.readthedocs.io/en/stable/colors.html
    title: Blessed documentation - colors
-
    ^merge: generic
    text: |
        Create an *abstract* class out of the `Board` class, and have it subclassed by `SimpleBoard` and `FancyBoard`. The first should work and display like before. The second should display a multi-colored board using the `blessed` library, and should allow the user to type coordinates like `A7` and `C2` instead of using the square number. This means that the `HumanPlayer` should delegate asking for input to the `Board` class, which can do so in a board-type specific way (using polymorphism). 

        On startup, have the user select the type of board.

    
Code quality:
-
    ^merge: codequality
    weight: 4
    bonus: 0
    malus: 0
    text: |
        OOP code quality guidelines:
        - Only use *private* attributes, with double underscores.
        - Methods that are only to be used internally by the class itself, should also be considered *private*.
        - Interact with instances of other classes not by reading/writing their attributes, but by calling a method, nicely asking the class to do something for you.
        - Put functionality in a method of the class that it (mostly) concerns. If it concerns multiple classes, see if you can split up the functionality. If it is related to a class, but doesn't use any instance attributes, make it a static method.
        - Try to prevent creating (too many) setters and getters, as they imply the actual functionality acting on this class is being implemented outside of the class.
        - Use abstract classes and abstract methods instead of *duck typing*.
