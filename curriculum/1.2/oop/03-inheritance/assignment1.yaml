- |
    In this assignment we will play around with _inheritance_, a key concept in object-oriented programming.
    
    Let's build a collection of shapes and colors to get a good understanding of the subject.

    <div class="notification is-info">
        The end result of what your application should output is shown at the bottom of this assignment. You might want to take a peek in case you get confused about what you're meant to achieve.
    </div>

-
    title: Shape
    ^merge: generic
    text: |
        Create a class named `Shape`. Every shape should be initialized with a size, an x position and a y position (pointing at the center of the shape). So one should be able to create an shape with size 100 at position x=200 y=0 like this: `Shape(100, 200, 0)`.

        When printing a shape (eg `print(my_shape)`) it should display something like: "shape of size 100 positioned at (200, 0)".

        In addition, it should have two *getter* methods:

        - `get_size()`, returning the shape's size, and
        - `get_position()`, returning a [tuple](https://www.programiz.com/python-programming/tuple) with the x and y position.

- 
    title: Square and Circle
    ^merge: generic
    text: |
        Shapes come in many forms and sizes. Create two subclasses of `Shape`: `Square` and `Circle`.

        Override their `__str()__` methods such that..
        
        - a `print(Square(100,0,0))` prints as "square of size 100 positioned at (0, 0)", and
        - a `print(Circle(100,0,0))` prints as "circle with radius 50 positioned at (0, 0)". (We interpret *size* here as the circle's diameter. The radius (Dutch: *straal*) of a circle is half its diameter.)

        Also implement `get_area` methods for both classes. They should calculate and return the [area](https://www.omnicalculator.com/math/area-of-a-circle) of the shape, based on its size. 

-
    title: Create shapes
    ^merge: generic
    text: |
        Within the `create_shapes` function...

        - Use a loop to create a *list* of 50 shapes, where each shape is (randomly) either a square or a circle, and has a random size between 1 and 100, a random x position between 0 and 800 and a random y position between 0 and 800.

- Polymorphism:
    - 
        link: https://www.geeksforgeeks.org/polymorphism-in-python/
        title: Polymorphism in Python
        info: The word polymorphism means having many forms. In programming, polymorphism means same function name (but different signatures) being uses for different types
    - 
        link: https://www.youtube.com/watch?v=CuK0g8OFzwo&list=PLsyeobzWxl7poL9JTVyndKe62ieoN-MZ3&index=62&ab_channel=Telusko
        title: Python Tutorial for Beginners | Duck Typing
        info: One of the ways polymorphism is used in Python is through the concept of duck typing. This video explains what that is.

-
    title: Print shapes
    ^merge: generic
    text: |
        Within the `print_shapes` function...

        - Iterate over the provided list of shapes, printing something like this for each shape: "I'm a circle with radius 35 position at (592, 73). My area is 3848." Note that this does *not* require an `if` statement. You should use polymorphism to have Python choose the appropriate `__str__`.
        - Print the total combined area of all shapes. Again, no `if` should be required.

-
    title: Create HTML
    ^merge: generic
    text: |
        For both `Square` and `Circle`, implement `get_css()` methods that return CSS strings that can be applied to a `<div>` element to have is displayed as the shape. You can use `position: absolute;` for position. You can pick a background color you like. Use 50% opacity to create semi-transparent shapes, so that you can see the shapes that would otherwise be hidden behind other shapes.
        
        For example:
        
        ```
        background-color: #db0651;
        width: 55px;
        height: 96px;
        position: absolute;
        left: 132px;
        bottom: 730px;
        border-radius: 50%;
        opacity: 0.5;
        ```

        Complete the `write_shapes_html` function to generate your work of art. The generated `shapes.html` should look something like this when opened in a browser:

        ![Intermediate work of art](shapes1.png)

        
- Super:
    -
        link: https://www.digitalocean.com/community/tutorials/python-super
        title: Python super() - Python 3 super()
        text: |
            How to use `super()` to call base class (also known as *superclass*) methods that have been overriden by a subclass.

-
    title: Rectangle
    ^merge: generic
    text: |
        Create a `Rectangle` class, subclassing `Shape`.
        
        - One should be able to instantiate a rectangle like this `Rectangle(x, y, width, height)`. This will require `Rectangle` to have its own constructor method.
        - `Rectangle`'s constructor should use `super()` to call the base class' constructor to initialize `Shape`s attributes. We'll (rather arbitrarily) define the `size` of a rectangle to be equal to its width.
        - Implement `get_width` and `get_height` methods. It is good practice never to store duplicate information, or information that can be derived from other information, in your class attributes. Therefore, `Rectangle` should not store `width` as an attribute, as that would duplicate the `size` of the `Shape`. (What would it mean if someone were to change `size` without changing `width`?)
        - Add `Rectangle` to your `create_shapes` function. Make sure `print_shapes` and `draw_shapes` now work for `Rectangle`s as well (without having to change these functions).

-
    title: Squares are rectangles too
    ^merge: generic
    text: |
        A square is just a special case of a rectangle. Change your `Square` implementation such that it's based upon `Rectangle`, reducing the amount of code as much as possible. You should not have to change anything in your `create_shapes` function.

-
    title: Pyramids
    ^merge: generic
    text: |
        Add another shape: the `Pyramid`.

        *Hints*:

        - The area of a pyramid is half the area of the square one could fit around it.
        - There's a [simple trick](https://www.freecodecamp.org/news/css-shapes-explained-how-to-draw-a-circle-triangle-and-more-using-pure-css/#triangles) to draw triangles in CSS.

-
    title: Rounded rectangles
    ^merge: generic
    text: |
        Add a new `RoundedRectangle` shape. It should work the same as a normal `Rectangle`, except that its constructor expects an extra *border_radius* argument.

        Add your new class to `create_shapes`, and make sure that it works with `print_shapes` and `draw_shapes`.

        Hints:
        
        - The maximum border radius should be half the width or half the height of the shape, whichever is smaller.
        - The area of a rounded rectangle can by calculated like this: width\*height - border_radius[²](https://favtutor.com/blogs/square-number-python) \* (4 - [π](https://www.w3schools.com/python/ref_math_pi.asp))

-
    title: Almost everything is a rounded rectangle
    ^merge: generic
    text: |
        Note that all of our existing shapes, except one, are special cases of a rounded rectangle. Use this fact to reduce the amount of code as much as possible.

        After this, you should only have two `get_css()` and two `get_area()` implementations left.

-
    title: Different colors
    ^merge: generic
    text: |
        Finally, we want each of the different types of shapes to have its own colors. For instance, all circles could be red while all squares are blue.

        We want to achieve this *without* each shape class having to implement its own `get_css()` method again, and *without* each instance having a color attribute.

        *Hint*: a base class (or superclass) can use polymorphism to call a method defined in a subclass.

- Expected output:
    - |
        Your console output should look something like this:

        ```
        I'm a rounded rectangle of width 146 and height 128 with border radius 29 positioned at (684, 144). My area is 17966.
        I'm a rectangle of width 195 and height 19 positioned at (439, 310). My area is 3705.
        I'm a rectangle of width 120 and height 76 positioned at (647, 726). My area is 9120.
        I'm a Pyramid with size 108 positioned at (171, 491). My area is 5832.
        I'm a Pyramid with size 63 positioned at (271, 580). My area is 1984.
        I'm a square of size 195 positioned at (207, 644). My area is 38025.
        I'm a square of size 20 positioned at (777, 615). My area is 400.
        I'm a Pyramid with size 84 positioned at (667, 446). My area is 3528.
        I'm a circle with radius 72.5 positioned at (280, 671). My area is 16575.
        I'm a rounded rectangle of width 16 and height 16 with border radius 1 positioned at (221, 687). My area is 255.
        I'm a Pyramid with size 97 positioned at (203, 361). My area is 4704.
        I'm a Pyramid with size 14 positioned at (296, 219). My area is 98.
        I'm a Pyramid with size 14 positioned at (591, 742). My area is 98.
        I'm a square of size 38 positioned at (147, 57). My area is 1444.
        I'm a square of size 122 positioned at (426, 403). My area is 14884.
        I'm a square of size 115 positioned at (647, 4). My area is 13225.
        I'm a square of size 169 positioned at (21, 748). My area is 28561.
        I'm a Pyramid with size 98 positioned at (663, 699). My area is 4802.
        I'm a square of size 135 positioned at (469, 727). My area is 18225.
        I'm a rectangle of width 51 and height 64 positioned at (231, 786). My area is 3264.
        I'm a rounded rectangle of width 126 and height 85 with border radius 32 positioned at (238, 371). My area is 9831.
        I'm a rounded rectangle of width 65 and height 47 with border radius 9 positioned at (366, 748). My area is 2985.
        I'm a rectangle of width 100 and height 92 positioned at (230, 707). My area is 9200.
        I'm a rectangle of width 139 and height 5 positioned at (358, 426). My area is 695.
        I'm a circle with radius 22.5 positioned at (126, 426). My area is 1610.
        I'm a rectangle of width 10 and height 50 positioned at (523, 277). My area is 500.
        I'm a rectangle of width 52 and height 14 positioned at (613, 534). My area is 728.
        I'm a circle with radius 43.5 positioned at (131, 319). My area is 5982.
        I'm a rounded rectangle of width 9 and height 7 with border radius 2 positioned at (426, 49). My area is 60.
        I'm a circle with radius 97.0 positioned at (87, 750). My area is 29559.
        I'm a rounded rectangle of width 2 and height 125 with border radius 1 positioned at (408, 158). My area is 249.
        I'm a square of size 70 positioned at (446, 765). My area is 4900.
        I'm a circle with radius 32.0 positioned at (347, 138). My area is 3217.
        I'm a square of size 1 positioned at (72, 355). My area is 1.
        I'm a rectangle of width 76 and height 32 positioned at (465, 132). My area is 2432.
        I'm a square of size 169 positioned at (772, 717). My area is 28561.
        I'm a rounded rectangle of width 97 and height 7 with border radius 0 positioned at (401, 569). My area is 679.
        I'm a rounded rectangle of width 15 and height 155 with border radius 7 positioned at (588, 267). My area is 2283.
        I'm a rectangle of width 59 and height 33 positioned at (371, 219). My area is 1947.
        I'm a rounded rectangle of width 109 and height 103 with border radius 27 positioned at (688, 379). My area is 10601.
        I'm a rounded rectangle of width 144 and height 43 with border radius 6 positioned at (532, 781). My area is 6161.
        I'm a square of size 68 positioned at (283, 89). My area is 4624.
        I'm a rectangle of width 135 and height 49 positioned at (347, 667). My area is 6615.
        I'm a square of size 182 positioned at (227, 700). My area is 33124.
        I'm a rectangle of width 65 and height 97 positioned at (586, 506). My area is 6305.
        I'm a square of size 15 positioned at (435, 316). My area is 225.
        I'm a Pyramid with size 92 positioned at (85, 375). My area is 4232.
        I'm a circle with radius 81.0 positioned at (227, 26). My area is 20612.
        I'm a rounded rectangle of width 173 and height 151 with border radius 1 positioned at (36, 185). My area is 26122.
        I'm a square of size 175 positioned at (306, 220). My area is 30625.

        Total area: 441360
        ```

        The generated `shapes.html` should look something like this:

        ![Final work of art](shapes2.png)


- Code quality:
    -
        ^merge: codequality
        weight: 4
        text: |
            OOP code quality guidelines:
            - Only use private attributes, with double underscores.
            - Methods that are only to be used internally by the class itself, should also be   considered *private*.
            - Interact with instances of other classes not by reading/writing their attributes, but by calling a method, nicely asking the class to do something for you.
            - Put functionality in a method of the class that it (mostly) concerns. If it concerns multiple classes, see if you can split up the functionality.
            - Try to prevent creating (too many) setters and getters, as they imply the actual functionality acting on this class is being implemented outside of the class.
