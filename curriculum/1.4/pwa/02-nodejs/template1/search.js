"use strict";

const express = require('express');
const expressHandlebars  = require('express-handlebars');
const htmlEntities = require('html-entities');

// Create an express web server application
const app = express();

// Configure `handlebars` to be used as the default template engine
app.engine('handlebars', expressHandlebars());
app.set('view engine', 'handlebars');

// Decode form post data.
app.use(express.urlencoded());

// The main route.
app.get('/', function(req,res) {
    // TODO! Replace this fake data with actual data.
    console.log('query:', req.query.terms);
    let page_count = 131,
        query = "software development",
        results = [
            {
                url: "https://developers.redhat.com/blog/2021/05/03/instant-replay-debugging-c-and-c-programs-with-rr/",
                title: "Instant replay: Debugging C and C++ programs with rr - Red Hat Developer",
                snippet: "...<strong>software</strong> Tools Link to developer tools for cloud <strong>development</strong> Events...",
            },
            {
                url: "https://news.ycombinator.com/itstrong?id=24949322",
                title: "Foundations of Software Engineering | Hacker News",
                snippet: "...codebase can make further <strong>software development</strong> a challenging process.",
            },
        ];

    res.render('home', {results, query, page_count});
});


// TODO: handle posts to /sites.
// Create a queue of pages to index, one at a time.
// Use the `node-fetch` NPM module to download the content for URLs.
// Use the provided `parseHtml` function to make sense of it.
// Depending on the given `depth`, add links in the document to the queue.


// Start the web server on port 3456
const listener = app.listen(3456, "0.0.0.0", function() {
    console.log(`Listening on port ${listener.address().port}.`)
});



/**
 * Crudely tries to extract text, title and links from an HTML string. Given the
 * simplistic approach, this won't always work correctly.
 * @param {String} html The HTML to parse.
 * @param {String} baseUrl The URL from which the HTML to parse was extracted. This is used
 *                         to create absolute URLs for links in case they were relative.
 * @returns An object containing `text` (a string), `title` (a string) and
 *          `links` (an array of strings) properties. 
 */
 function parseHtml(html, baseUrl) {
    let text = html
        .replace(/<script[^>]*>[\s\S]*?<\/script[^>]*>/gi, '')
        .replace(/<style[^>]*>[\s\S]*?<\/style[^>]*>/gi, '')
        .replace(/<[^>]*>/g, ' ')
        .replace(/\s+/g, ' ')
        .trim();
    text = htmlEntities.decode(text);

    let match = html.match(/<title[^>]*?>([\s\S]*?)<\/title[^>]*?>/i);
    let title = match ? htmlEntities.decode(match[1].trim()) : '?';

    let links = [];
    for(let match of html.matchAll(/<a[^>]* href=['"]([^'">]*)['"]/g)) {
        let url = new URL(htmlEntities.decode(match[1]), baseUrl).href.split('#')[0]
        if (["http","https"].includes(url.split(":")[0])) links.push(url);
    }

    return {text, title, links};
}