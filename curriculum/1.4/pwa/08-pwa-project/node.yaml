name: PWA Project
description: Implement front-end, backend-end and testing for a PWA of your own design.
days: 3
ects: 5
type: project
public: users
miller: sh
allow_ai: true
goals:
    javascript:
        apply: 1
        title: Program in JavaScript.
    nodejs:
        apply: 1
        title: Use the Node.js ecosystem, including packages and build tools.
    spa:
        apply: 1
        title: Create single-page applications in JavaScript.
    rest:
        apply: 1
        title: Build tidy REST APIs using Node.js and express.js.
    dom:
        apply: 1
        title: Manipulate the browser DOM to add interactivity to web sites.
    svelte:
        apply: 2.0
        title: Use Svelte to implement a web app.
    pwa:
        apply: 2
        title: Create (progressive) web apps that install as native apps and work offline.
    apitests:
        apply: 0.75
        title: Create API tests using Cypress.
    e2etests:
        apply: 0.75
        title: Create end-to-end tests using Cypress.
assignment:
    - |
        Create the frontend and backend for a single page application. What the application does is up to you. The technical requirements are detailed in the objectives below.

        The provided template is the same as for the previous assignment. Feel free (not) to use it.

    - |
        **Note:** This is an exam *project*. As opposed to regular exams, you're allowed to:

        - Get a bit of help from the teachers.
        - Talk about the project with your class mates, and show them a demo.
        - Keep your source code, and do with it whatever you like. (Continue working on it? Open Source it? Impress your future employer? Build a billion dollar business around it?)

    -
        ^merge: feature
        title: REST backend
        text: |
            Design a REST API and implement it using Node.js and express, taking best practices for REST API design into account. Data should be stored in a database.

            The API should consist of at least 8 routes.
        map:
            javascript: 1
            nodejs: 1
            rest: 1

    -
        ^merge: feature
        title: REST backend tests
        code: false
        text: |
            Create a few automated API tests for the most error-prone parts of your REST API using Cypress. You're *not* required to fully test your API, just the parts that you think would benefit most.

            As with regular code, your test code should use abstraction in order to prevent too much duplication.
        map:
            apitests: 1

    -
        ^merge: feature
        title: Svelte frontend
        text: |
            Use Svelte to create a web app frontend to your REST backend. 

            Your frontend should offer at least as many features as the to-do list app from the *Svelte* lesson, and preferably a bit more functionality. You will come up with a much more original application than a to-do list, of course.
        map:
            javascript: 1
            spa: 1
            svelte: 1

    -
        ^merge: feature
        title: PWA
        text: |
            Your application should be *installable* on mobile phones, using a nice icon of your choice and a custom title bar color on Android.

            Also, your web app should be able to (partially) work while offline.
        map:
            pwa: 1

    -
        ^merge: feature
        title: End-to-end tests
        code: false
        text: |
            Create a few automated end-to-end tests for the most error-prone parts of your web app using Cypress. You're *not* required to fully test your app, just the parts that you think would benefit most.

            As with regular code, your test code should use abstraction in order to prevent too much duplication.
        map:
            e2etests: 1

    -
        ^merge: feature
        title: Advanced features
        map:
            javascript: 1
            dom: 1
            pwa: 1
        text: |
            Your web app should make use of at least 3 of the following:

            - HTML5 *canvas*.
            - *prefers-color-scheme* to show your app in *dark mode* if the user has dark mode configured at the operating system level.
            - *WebSocket* for real-time communication. (This counts for 2.)
            - A third-party API, either from your backend or from your frontend. (This counts for 2 if an authentication scheme like OAuth is involved.)
            - The *Web Share API* to allow the user to easily share something from your app through other apps (such as WhatsApp and Gmail).
            - The *Media Session API*. 
            - *WebRTC* to share real-time video, audio and/or data with another browser without going through a server. (This counts for 2.)
            - The *Gamepad API* to control your app using a game controller.
            - The *Generic Sensor API*.

    -
        must: true
        title: Layout
        text: |
            Your web app must have a consistent, decent-looking layout.
