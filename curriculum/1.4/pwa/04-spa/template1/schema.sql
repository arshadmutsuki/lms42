create table if not exists lists(
  id integer primary key,
  name text not null
);

insert or replace into lists(id,name) values(1, 'Groceries');
insert or replace into lists(id,name) values(2, 'To the moon!');

create table if not exists items(
  id integer primary key,
  listId integer not null,
  name text not null,
  checked int not null default 0 /* 0=false 1=true */
);

insert or replace into items(listId,id,name) values(1, 1, 'Milk');
insert or replace into items(listId,id,name) values(1, 2, 'Sugar');

insert or replace into items(listId,id,name) values(2, 3, 'Build a rocket');
insert or replace into items(listId,id,name) values(2, 4, 'Bring some groceries');
