name: Assembly & the stack
description: Let's implement functions using a call stack in SAX16 assembly!
allow_longer: true
pair: true
goals:
    memstack: 1
    computerarchitecture: 1
    pointers: 1
    asm: 1
assignment:
    Assembly languages:
    - |
        You may have wondered (or maybe not 😀) how the teachers created the various `.executable` files in the previous lesson. Surely not by typing all these hex codes in a hex editor? Rest assured, your teachers are mere mortals. We used an assembly language for that.
        
        Assembly languages are programming languages that are conceptually *very* close to the instructions read by the computer. For every instruction (and its operands) there is usually a single line of assembly code. This line contains the name of the instruction in human readable form, as well as some human readable (depending on your definition of 'human') representation of the operands.

        As different CPU architecture (like AMD64 (your laptop), ARM (your phone) and SAX16 (your nightmares 😀)) use different instruction sets and different ways of handling their operands, the assembly language for each CPU architecture is different. This is one of the reasons not many programmers code in assembly nowadays: it makes porting software to other platforms very difficult. The other big reason **not** to write assembly but to use some higher-level language like C, Rust or even Python, is that writing programs in assembler takes a *lot* more work, is a *lot* more difficult and has a *lot* more potential for bugs. Still, people sometime *do* write assembly, for instance if maximum performance is of the utmost importance. Writing machine code that is faster than what a good modern compiler for a higher-level language can write requires a lot of expertise though.

        Today, we'll be learning SAX16 assembly. It's a bit simpler than other assembly languages, but has all of the same considerations.

    SAX16 assembler: 
        Instructions: |
            The names of the thirteen instructions should come as no surprise, as you've already encountered them in the previous assignment: set, add, sub, mul, div, and, or, xor, if=, if>, if<, read, write. However, in SAX16 assembly we'll suffix each instruction name with the word size for the instruction in bits: 8, 16 or 32. For instance we can write `read8`, `if=16` or `mul32`.
            
        Operands: |
            What should come after the instruction name on the same line are the operands. Each of the one or two operands can take one of these forms:

            - A literal decimal number, like `0` or `123`.
            - A single character within single quotes, like `'a'` or `'@'`. This does nothing more than converting the character to its ASCII code, which is used as a literal number.
            - A memory read with a single indirection, indicated by an asterisk in front of the (decimal) address. For example: `*123`. The operand is the value of what is at memory address 123.
            - A double indirection, using two asterisks. For example: `**456`.  The operand is the value of what is at the memory address given at memory address 456.

        Data: |
            Besides instructions, a program can contain data. SAX16 assembly supports two forms in which you can provide data:

            - Using one of the `int8`, `int16` or `int32` keywords you can add an unsigned integer of the given size to the program. For instance `int8 64` would add the number 64 as a single byte, and `int32 123456` would add 123456 as four bytes. 
            - As a double quoted string. For instance `"test123"` would add these 7 characters as 7 bytes to the program.

            ```asm
            int8 123
            int16 456
            "Hello world!\n"
            ```

        Labels: |
            Manually typing the numeric values for addresses as operands would be an exercise is frustration, as it would involve manually keeping track of which data is at what address, and it would mean that inserting some data or some instructions in your program would change the addresses for all data and instructions that come after, requiring you to update all operands that refer to these addresses. Also, it's hard to determine which instruction is on which address is just by looking at assembly code.

            Therefore, assembly languages use labels. A label is defined by just a name followed by a colon, and can be used by just typing that name in place of where you'd normally type a number in an operand. Like this:

            ```asm
            my_counter: int8 'a'

            write8 *my_counter 
            add8 *my_counter 1
            ```

            Labels can also be used as initialization values of `int16` data. In the example below, 6 bytes of data will be allocated. The first two bytes contain a 16-bit integer that holds the address of `test_string` (which would be 0x0002). The `current_address` integer could be used (and modified) by the program to loop over the string, for instance.

            ```
            current_address: int16 test_string

            test_string: "test"
            ```

            Labels can also refer to the first of a list of instructions. Note that the indentation and the (lack of) use of new lines after a label definition are only there to help readability.
            ```
            my_instruction:
                add8 3 4
                etc...
            ```

        The instruction pointer: |
            As you may remember, the 16 bits at address 0 of a program are the *instruction pointer*, the address at which an instruction will be read and executed next. SAX16 assembly automatically starts every program with this line:

            ```asm
            ip: int16 main
            ```

            That means that a label named `ip` is created that will refer to address 0. At address zero will be a 16-bit integer (the instruction pointer!), that will initially have the address of the `main` label as its value. The main label hasn't been defined yet though - it's up to the program itself to define the main label before the instruction it wants the CPU to start.

        Comments: |
            Like Python, everything on a line after a '#' character is ignored by the computer; it's just comments.


        An example: |
            A full program might look like this:

            ```asm
            # ---- data ----
            hello_start:      # The address of the first by of the hello world string
            "Hello world!\n"  # The hello world string itself, including new line
            hello_end:        # The address of the first byte after the string
            
            print_address:    # The address of a 16-bit integer, defined next
            int16 hello_start # 16-bit integer that initially holds the address
                              # of the first byte of the string
                
            # --- code ----
            main:             # This is the address that ip will initially hold, so this
                              # is where the CPU starts executing

            if=16 *print_address hello_end  # Is the address at print_address past the
                                            # end of the string?
            set16 *ip 0             # Then halt the program - set the instruction pointer to 0

            write8 **print_address  # Write the current byte - note the double indirection!
            add16 *print_address 1  # Increment the address at print_address
            set16 *ip main          # Jump back to the main label
            ```

            Try to create an executable from it using the provided `asm.py` assembler. 

        Macros: |
            Assembly code will always be a lot more verbose (lengthy!) than code in a higher level language doing the same. But in order to make assembly programming bearable (if only just), assemblers usually support *macros*.

            Macros allow you to easily insert a named set of instructions at any point in your code, while substituting arguments. Superficially, this is really similar to the use of functions, except that for macros an actual copy of the instructions is added to the executable program every time the macro is invoked. Overuse of macros can create needlessly large executables. Also, any (indirect) recursion is *not* possible using macros, as it would attempt to create an infinitely large executable.

            Here's an example of a macro definition and usage in SAX16 assembler:

            ```asm
            a: int32 0
            b: int32 0

            # A macro without arguments
            macro someNonsense {
                add32 *a 3
                mul32 *a *a
            }

            # A macro with two arguments
            macro moreNonsense FOO BAR {
                add32 *a FOO
                set32 BAR *a
                set16 *ip 0
            }

            main:
                # Include someNonsense macro twice
                someNonsense
                someNonsense

                # Include the moreNonsense macro, where FOO=123 and BAR=*b
                moreNonsense 123 *b
            ```

        Assignment:
            -
                ^merge: feature
                code: 0
                text: |
                    Create a program in SAX16 assembly that allows the user to type a number followed by enter (which will show up as a '\n' new line character). After that the program prints that number of '.' characters and a new line character. The program should repeat this whole cycle 3 times, and then halt.

                    Write your solution in the provided `number.asm`, which is intentionally empty. The SAX16 assembler is a Python program that is provided as `asm.py`. You can run it using `python asm.py number.asm`, and if all goes well it will deliver a `number.executable` as well as a program dump on the console that you can use to spot problems. If you don't trust your own virtual machine implementation to do the right thing, you can use the provided one like this: `./vm number.executable`. Use `./vm -d number.executable` for a report on each instruction that was executed.

                    Hint: reading a number one digit at a time is relatively easy. In pseudo-Python:
                    ```python
                    num = 0
                    while True:
                        char = read() # Read a byte from the console; not actual Python.
                        if char<'0' or char>'9': # Something other than a digit?
                            break # Stop reading!
                        num = (num * 10) + (char - '0')
                    ```

                    Hint 2: Note that the `read8` instruction will wait for input and deliver the ASCI code for one character at a time. However, due to the way the Linux console processes input, characters will only be sent as a batch once the user presses enter. 
                    
                    Hint 3: if you want to see more SAX16 assembly examples, you can have a look at the template files provided for the other two objective. These are not empty.

    Registers: |
        Most CPU architectures have registers. Each register is a very fast piece of memory than can store one *machine word*. So depending on the machine that can be a 16-bit, 32-bit, 64-bit or 128-bit integer. CPU architectures often require the programmer (or the compiler) to load data from RAM into a register before it can perform any further operations on that data.

        CPU's usually have only about 10 registers that are commonly. Some of these are *general purpose* registers, to be used by the programmer as she/he pleases. And some are *special purpose* registers - they have a specific meaning to the processor. The *instruction pointer* is one such special purpose register.

        To keep things simple, the SAX16 architecture has no registers. Even the *instruction pointer* is just an address (0x0000) in RAM. It may be convenient to create some *general purpose* data allocations in RAM as well, to act as general purpose registers. They would just be 32-bit integers named `a`, `b`, `c`, etc. This would allow different parts of your program to reuse the same memory, instead of each part having its own 'variables' permanently reserved, even when they're not needed.

        In case you're wondering if using 'variables' named `a`, `b` and `c` wouldn't be terrible for code readability: yes, indeed! However, when writing in assembly, programmers don't get to enjoy luxuries like properly named variables. It is therefore advisable the comment heavily, in order to explain what code is supposed to be doing. Also, function (as we'll discuss next) should thoroughly document in which registers they expect to receive which arguments, which registers are used to store which result value, and which registers will be 'destroyed' as they're used as temporary variables.

    The stack:
        -
            link: https://www.youtube.com/watch?v=5xUDoKkmuyw
            title: The Call Stack
            info: This video starts out easy by providing a general idea of what the call stack is based on an example in Python. You'll learn some new terms (that we'll use!), but nothing here should be all that surprising.
        - |
            As should be clear from the video above, we need a stack in order to create functions for our SAX16 machine. Up til now, we have just been changing the instruction pointer, but the code that gets called has no way of knowing to what instruction to *return* after it is done.

            So before jumping to the start address of a function, the caller should *push* the return address onto the stack. The return address could just be determined by means of a label, or it could be calculated based on the current *instruction pointer*.
            
            When a function is ready, it *pops* the return address from the stack, and jumps to it.

        -
            title: Two functions
            ^merge: feature
            code: 0
            text: |
                Implement the `printString` and `prompt` function.
                
                The `printString` function receives the 16-bit address of a string in general purpose variable `a`. The string should be zero-terminated, meaning that a 0-byte comes immediately after the string in memory, so that the function knows when it has reached the end of the string. The function should write the string to the console, and return to the caller by popping the return address from the stack and jumping back to that address.

                The `prompt` function also receives a string address in `a`. It should print that string using `printString`. Next it should read a number from the console, like in the previous objective. It puts this number in the general purpose `a` register. Finally the function should return to back to the caller.

                In the provided `prompt.asm` file, the registers, a stack, a stack pointer (`sp`), a `call` macro (that calls a function) and a `main` have already been created. Try to understand how all this works!
        - |
            Besides return addresses for functions, the stack can also contain data. Many common programming languages allocate all their function-local variables on the stack, for instance. So in order to load a local variable, the program would have to read from something like `*(*sp + 20)`. Many CPU architectures have specific operand types that do just that as part of a single instruction. Our SAX16 architecture does not, so let's stick to the use of global data (and 'registers') for our variables.

            However, what to do when a function wants to call another function, that uses (or 'destroys') the registers the calling function wants to preserve? For instance when a function recursively calls itself? For that, we can *push* registers (or other data) to the stack to preserve them, before calling a function that will reuse these registers. After the function returns, we need to make sure we *pop* the stack values back into their registers. Remember that a stack works LIFO (Last-In-First-Out).

        -
            title: Data on the stack
            ^merge: feature
            code: 0
            text: |
                Implement these two macros:

                - `push32`: Pushes its 32-bit *argument* to the stack.
                - `pop32`: Loads a 32-bit value from the stack into its *argument*.

                They should store/retrieve a value to/from the stack and modify the stack pointer. Think carefully about the order.
                
                Making use of these macros, implement the `fib32` function to recursively calculate the Fibonacci number. In Python:

                ```python
                def fib32(n):
                    if n < 2:
                        return n
                    return fib32(n-1) + fib32(n-2)
                ```

                You can use the provided `fib.asm` to start with.

                *Hint:* The `fib32` function shouldn't *destroy* any registers (other than its output register `a`), meaning their values should be the same when returning from the function as they were when entering the function. You can, however, use these extra registers as temporary variables, by pushing their initial values to the stack and popping them back before returning.
