- |
    We're going to be applying the CSS styling for a Thuisbezorgd.nl page. Given the (simplified) HTML for a search results page, it's up to you to implement most of the CSS.
- Make it match:
    text: |
        Try to make the site match the image below as exact as you can. You may **only** edit the `style.css` file - the HTML should be left untouched. 

        ![example](example.png)

        We recommend that you open the above image in a new tab (right click &rarr; view image). By opening the `index.html` in an adjacent tab, you can easily spot the difference by switching between them.
    map:
        css: 2
    0: Barely better than the template.
    2: Many small problems and/or at least one large problem, causing the site to be obviously unsuited for production.
    3: One or two small problems. The rest is almost pixel perfect.
    4: Only differs by a couple of pixels margin here and there. Everything looks like intended.

- Let the browser do the calculations:
    must: true
    text: |
        When first learning CSS, it can be tempting to use rules like `margin-left: 163px;` and `padding: 23% 7% 0 15px;` to get things to layout exactly the way you want, using trial and error to arrive at the right amount. That's generally a bad idea though, as it will create a very fragile and difficult to adapt web site. Usually it means things will look wrong when people are viewing your website on a browser window that is a little bit wider or narrower than yours.

        A better solution is to have *the browser* calculate where exactly everything should be on the screen, by specifying how to should relate to each other. The *Flexbox* is a valuable tool for this. Remember that you can use it vertically as well as horizontally. Of course, you'll still need to specify some margins and paddings (generally in `px`). But these will usually be applied to both the left and right side and/or both the top and bottom side of an element. Also, they will often be very consistent, using just multiples of `15px` for instance.

        In this assignment, try to apply this technique where you can. One exception is the size of the restaurant logos, which you may need to give a very specific width and/or height.

- CSS readability:
    ^merge: codequality
    text: |
      CSS is not only meant to be read by computers, but by humans as well. Your coworkers, and perhaps you yourself, a couple of months from now.

      That's why 'code quality' is important when writing CSS. Your CSS should be:
      - Indented consistently.
      - Well-organized, keeping relevant parts together.
