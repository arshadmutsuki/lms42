Assignment: |
    Based on a set of screenshots, recreate an existing website that looks good on screens of widths ranging from 320px (tiny mobile) up to 1920px (full HD). For this, you'll need to apply many of your skills from the lessons in this series so far.

    The website we'll be recreating is a somewhat simplified version of <b>www.nos.nl</b> - a Dutch news site. In the screenshots below you can see what the site should look like for various browser widths. The widths in between should of course look good as well. Click on the thumbnails below to view the full screenshots. 

    <style>
    .responsive-examples {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px;
    }
    .responsive-examples > a {
        display: flex;
        flex-direction: column-reverse;
        justify-content: flex-end;
        width: 324px;
        padding: 12px;
        color: inherit;
    }
    .responsive-examples > a > .img {
        width: 300px;
        height: 300px;
        background-size: 300px;
        background-position: top center;
        background-repeat: no-repeat;
        box-shadow: 0 2px 5px rgba(0,0,0,0.3);
        margin-bottom: 6px;
    }
    </style>
       
    <div class="responsive-examples">
        <a target="_blank" href="6.png"><figcaption>The content is centered and exactly 1200px wide.</figcaption><div class="img" style="background-image: url(6.png);"></div></a>
        <a target="_blank" href="5.png"><figcaption>The layout scales down until a viewport width of 800px, maintaining `24px` margins on all sides.</figcaption><div class="img" style="background-image: url(5.png);"></div></a>
        <a target="_blank" href="4.png"><figcaption>Below 800px there's only one featured article (the other one becomes a regular article) and there are only 3 columns below that.</figcaption><div class="img" style="background-image: url(4.png);"></div></a>
        <a target="_blank" href="3.png"><figcaption>Notice how the header is simplified once we go below 700px.</figcaption><div class="img" style="background-image: url(3.png);"></div></a>
        <a target="_blank" href="2.png"><figcaption>Below 600px, there are only two columns of regular articles.</figcaption><div class="img" style="background-image: url(2.png);"></div></a>
        <a target="_blank" href="1.png"><figcaption>Below 400px, there is only one column left and the header is further simplified. This should still look right on 320px.</figcaption><div class="img" style="background-image: url(1.png);"></div></a>
    </div>

    Try to get your site to match the examples as closely as possible, preferably pixel perfect, but at least make sure margins are consistent and alignments are correct! A useful trick to spot any differences is to open an example screenshot in one browser tab, and your website in another. 

    Your page does not need to handle clicks on links. *Do* make sure that the header bar and its drop shadow are fixed to the viewport when the page is scrolled. Also, mobile user should *not* be able to zoom the page.

A couple of hints: |

    - Colors used are:
        - <div style="background-color: #e61e14; padding: 3px 6px; color: white; display: inline-block;">#e61e14</div>
        - <div style="background-color: #555; padding: 3px 6px; color: white; display: inline-block;">#555</div>
        - <div style="background-color: #ddd; padding: 3px 6px; color: black; display: inline-block;">#ddd</div>
        - and black and white.
    - All marges and padding are multiples of `6px` (mostly `12px` and `24px`).
    - All text uses the *Arial* font family with default `font-size`, except the featured articles, which have a 1.5 times larger font and a font weight of 700.
    - You probably want to use CSS *grid* to layout the articles. Although perfect implementations using *flexbox* or even *float* or *inline* may be possible, they will be more fragile and tricky to get right.
    - It may be a good idea to implement the *featured articles* (the big images on top) last.
    - In order for articles to sometimes (depending on your CSS media queries for screen width) render as a *featured article*, you will want to find a way to do that with the exact same HTML as for the regular articles. The use of `position: absolute;` may come in handy here, to position the title text *on top of* the image.
    - It's easiest to use a [background-image](https://www.freecodecamp.org/news/css-full-page-background-image-tutorial/) property for the article images, instead of `<img`>s. As the image is semantically part of the document, it's a good practice (and convenient) to use [inline styling](https://www.freecodecamp.org/news/inline-style-in-html/) for setting the URL of the background image. All other CSS properties should be in the stylesheet though!
    - To make special rules for the first one or two articles, you can use the [:nth-child() pseudo-class](https://developer.mozilla.org/en-US/docs/Web/CSS/:nth-child) instead of having to add more (semantically meaningless) classes to the HTML.
    - The images for featured articles have a gradient overlay to make the white titles more readable. The gradient is fully transparent in the top half of the image, and linearly goes from fully transparent in the middle to 50% black at the bottom of the image.
    

Criteria:
    Matches the example:
        map:
            responsive: 3
            css: 1
        0: This looks nothing like the original and is not usable at many browser widths.
        2: There are some large flaws, rendering the site unpleasant to work with at some browser widths.
        3: Roughly the same as the original at any browser width, with one or two minor flaws.
        4: Could easily be mistaken for the original at any browser width.
    Understandable and correct CSS & HTML:
        ^merge: codequality
        malus: 0
        bonus: 0
        map:
            css: 1
            html: 1
