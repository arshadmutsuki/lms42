Create a database:
- |
    We are going to implement a database for storing data about a curriculum of a study programme.
    
    ```plantuml
    hide circle
    skinparam linetype ortho

    entity lessons {
        +name
        +days
        +type
        description
    }

    entity modules {
        +name
        description
    }

    lessons }|-|| modules

    entity learning_goals {
        +title
        description
    }

    lessons }|--|{ learning_goals

    entity programmes {
        +name
        +years
    }

    modules }|--o| programmes

    entity enrolments {
        +start_date
        end_date
    }

    programmes ||-o{ enrolments

    entity students {
        +name
    }

    enrolments }o-|| students

    entity addresses {
        +street
        +city
        +country
    }

    students }|-|{ addresses
    ```

-
    link: https://www.tutorialspoint.com/postgresql/postgresql_create_table.htm
    title: PostgreSQL - CREATE Table

-
    link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-data-types/
    title: PostgreSQL Data Types
    info: You'll need to pick an appropriate data type for each of your columns. This article explains what you can choose from. You can ignore the last four sections (Arrays, JSON, UUID, Special data types) for now.

-
    text: Within `assignment.md`, write all `CREATE TABLE` (and similar) queries necessary to setup a PostgreSQL database that matches the above logical ERD but without adding the relationships. It's up to you to figure out data types. Also, make your columns `NOT NULL` when appropriate.
    0: No tables.
    1: Two correct query.
    2: Four correct query.
    3: Six correct query.
    4: All tables are correct.

-
    link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-primary-key/
    title: PostgreSQL Primary Key
-
    link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-serial/
    title: Using PostgreSQL SERIAL To Create Auto-increment Column
-
    link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-foreign-key/
    title: PostgreSQL Foreign Key
- |
    ### Mapping ERD to SQL schemas

    - Each entity (such as `Student`) in the diagram is a table in SQL.
    - Each property (such as `birthdate`) within an entity is a column in the corresponding table.
    - Required properties are `NOT NULL` in SQL terms.
    - How a relationships between two entities, for example *Persons* and *Cars*, translates into SQL depends on the multiplicities:

    | Multiplicities | Example | SQL |
    | -- | -- | -- |
    | `one-to-many` | Each car is owned by exactly one person; a person can own many cars. | A column and foreign key referencing *Persons* must be added to *Car*.|
    | `many-to-one` | Each person can have one favorite car; a car can be many people's favorite. | A column and foreign key referencing *Cars* must be added to *Persons*.|
    | `one-to-one` | At a single time, each person can drive just one car; each car can be driven by just one person. | Like `one-to-many` or `many-to-one`, but the added column must be made `UNIQUE`.
    | `many-to-many` | A person can *like* many cars; a car can be *liked* by many people. | A junction table referencing both a car and a person must be created. It will typically have just the two reference columns, two foreign key, and a primary key that consists of the combination of both columns. |

    In the above table, `one` should actually be read as `zero or one`. In cases where zero should not be allowed, the foreign key column should be made `NOT NULL`. Also, `many` should be read as `zero or more`. A bit of nuance is lost-in-translation here - we can't easily express a `one-to-(one-or-more)` relationship in SQL.

-
    text: |
        Now it's time to create the relationships in your database, as modeled in the ERD. You'll need to add (auto-incrementing) `id`s, primary keys and foreign keys.
    
        You can do this by extending the `CREATE TABLE` statements you created in `assignment.md` for the previous objective.

        *Hints:*
        
        - A table must exist before it can be referred to by a foreign key. Therefore, you may want to reorder your `CREATE TABLE` statements.
        - There are two *many-to-many* in the ERD, for which you'll need to create *junction tables*. A junction table has two foreign keys (linking rows from two tables together). A junction table usually doesn't have an `id` column itself. Instead, the primary key consists of the two foreign keys together, as a link is guaranteed to be unique. (A student can't enroll for the same programme twice.)
        - The footnotes in the output of commands such as `\d your_table_name` show you which primary and foreign keys are defined on the table.
    weight: 2
    1: Half the foreign keys are right.
    3: All foreign keys are perfect. A couple of primary key/auto-incrementing flaws.
    4: All primary keys and foreign keys are perfect. `id`s are auto-incrementing.

Insert data:
-
    text: |
        Let's add some data to our database, based on our very own study programme. Insert the following `modules` using a single query, leaving the `programme_id`s `NULL` for now.

        | Title | Description |
        | -- | -- |
        | Coding in Python | Python is a programming language that is relatively easy to use for beginners, but commonly used by experts as well. It is a general purpose language, meaning it can be used to create many types of computer programs. It is often used for quickly automating tasks, data analysis, artificial intelligence and for creating web applications. In this module, we'll learn the basics of imperative programming using Python. |
        | Static web | At its core, the world wide web consists of a collection of documents that link to each other. The documents are written using HTML and styled using CSS. Both are languages intended to be understood by computers, but are not programming languages. We will learn these languages to be able to create decent-looking web pages. Later on, we'll build on this knowledge to create web applications. |
        | SQL | Applications usually work on (large amounts of) information. Such information is often stored in a database that is controlled by a database management system. The system we'll be working with is PostgreSQL. Like other well-known database systems (such as Oracle, MySQL and PostgreSQL) one can interact with it using the SQL computer language in order to store and retrieve data. We will be learning how to represent real-world objects as an SQL database, and how to efficiently generate all kinds of useful reports from this data. |

        Next, insert a `programme` named "Associate degree Software Development" that has a duration of 2 years. After that, use an `UPDATE` to set the `programme_id` for all (three) modules to the `id` of the `programme` that was just inserted. (*Hint:* use `LASTVAL()`.)

        There should be three queries in total, that you should add to `assignment.md`.
    weight: 0.5
    1: No data has been imported.
    2: Some data has been imported.
    3: All data has been imported but not not all relations are correct.
    4: All data and relationships are correctly imported.

Import data from a file:
-
    link: https://www.postgresqltutorial.com/postgresql-tutorial/import-csv-file-into-posgresql-table/
    title: Import CSV File Into PostgreSQL Table
-
    text: |
        Next, we want to import the data from the `import-lessons.csv` file into the database. A CSV file contains [Comma-Separated Values](https://en.wikipedia.org/wiki/Comma-separated_values). It's often used for importing and exporting data into databases, spreadsheets and many other types of programmes. Take a look at the contents of the file in Visual Studio Code. It should be easy to understand.

        As you can see, there's a slight problem though: the CSV file *almost* matches the `lessons` table, only it includes a `module_name` column instead of a `module_id` column (or you might have named this column slightly differently). So we'll need to convert the names to ids. Here are the steps to take:

        1. Create a temporary table with columns that match the CSV files exactly. As this is just a temporary table, there's no need to worry about things like primary/foreign keys and `NOT NULL`.
        2. Use a `COPY` query to read the CSV file into the temporary table. The filename should be relative to the data directory of the PostgreSQL server, which is `data/postgresqlite/pgdata` as seen from the assignment directory. Therefore, for PostgreSQL the relative path of the CSV file is `../../../import-lessons.csv`. You'll need to tell the `COPY` query that the file is a CSV file, and that it's using semicolons (`;`) to separate fields.
        3. Use a `INSERT INTO ... SELECT` query to convert the data in the temporary table to fill the `lesson` table.
        4. `DROP` the temporary table.

        Put the four queries in `assignment.md`.
    2: Didn't work, due to one small problem
    4: Perfect


Extend the database:
-
    text: |
        Included in the assignment is a file called 'import-grades.csv'. This file contains the grades from certain students for the exams for some of the modules. Your task is to store this information into the database. Like above, you need to import the data into a temporary table (that matches the CSV file exactly) first. Also, you should create the required tables, columns and relationships for properly storing the data in the database. Be sure to *normalize* the data.

        You may assume that all students have enrolled into the *Software Development* programme on 2022-09-01 and that none of them have finished yet.

        For importing the data use the strategy as described before (first directly import the CSV in a temporary table called *import_grades*). Don't forget to save these queries into the *commands.sql* file.

    0: Database was not extended.
    1: Incorrect extension of database with no import.
    2: Incorrect extension of database with some incorrect data import.
    3: Correct extension of database but not all the data or relations are imported correct.
    4: Correct extension of database including correct data and relationships.
    weight: 2.0

Alter to normalize:
-
    link: https://www.lifewire.com/normalizing-your-database-first-1019733
    title: Putting a Database in First Normal Form
-
    text: |
        Within the `lessons` table there is a column that's rather repetitive. It has not been properly normalized. Normalize it by:
        
        1. Creating a new table for it.
        2. Inserting all unique rows to the new table based on what's in the `lessons` table.
        3. Adding a column to the `lessons` table that references the new table. The column should allow `NULL` values.
        4. Updating the new column so that each row points at a row in the newly created table.
        5. Dropping the original (repetitive) column.

    2: 50% of the way towards solving.
    4: correctly solved.

