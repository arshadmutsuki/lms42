name: Join the fun
goals:
    joins: 1
resources:
    -
        link: https://code.tutsplus.com/articles/sql-for-beginners-part-3-database-relationships--net-8561
        title: "SQL for Beginners: Part 3 - Database Relationships"
        info: Here you will learn how to work with multiple tables that have relationships with each other. First, some core concepts are explained, and then the JOIN queries in SQL.
    -
        link: https://www.youtube.com/watch?v=FPqXPkQkMP0
        title: Easy Way to Understand Inner vs Outer Joins in SQL
        info: This guide walks through the key differences between inner and outer joins in SQL. Additionally, we will examine inner joins, outer right joins, and outer left joins. We'll also walk through examples of both inner and outer joins in SQL.

assignment:
    Assignment:
        - |
            We'll continue our deep dive into the wonderful world of DVD rentals, by combining the data from multiple tables using `JOIN`.

        - SQL coding conventions: |
            When using `JOIN`s please write your queries in the following form:
                
            ```sql
            SELECT t.first_name, t.last_name, s.name
            FROM teacher t
            JOIN skill s ON s.teacher_id = t.id
            WHERE t.level = 'Awesome'
                AND t.years_experience > 3
                AND s.name LIKE '%Programming%'
            ORDER BY t.first_name
            LIMIT 2
            ```

            So, in addition to the conventions mentioned in the previous assignment:
            * You should put each `JOIN` (including the `ON` part) on its own line.
            * For a long `WHERE` condition, you may put each `AND`s clause on its own line, with a bit of indentation.

        - Important notes: |
            - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
            - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to show all info for *Frank*, you can't do something like `SELECT * FROM teachers WHERE id=42` but you need to do something like `SELECT * FROM teachers WHERE name='Frank'`.


    Joining two tables:
    -
        link: https://learnsql.com/blog/how-to-join-tables-sql/
        title: How to JOIN Tables in SQL
        info: This tutorial starts with a quick example, and then proceeds to explain the details step by step. It only covers regular (`INNER`) `JOINS`, which is enough for now.
    -
        link: https://www.youtube.com/watch?v=9yeOJ0ZMUYw
        title: SQL Joins Explained
        info: Video that visually explains the various types of `JOIN`s. Although it's good to get an overview of the various types of `JOIN`s, you can focus on the `INNER JOIN` (which is the default type of `JOIN`) for now.
    -
        link: https://www.postgresql.org/docs/current/tutorial-join.html
        title: PostgreSQL documentation tutorial - Joins Between Tables
        info: In case you're struggling to grasp the concept, here's another good resource. It's an introduction on `JOIN`s from the official PostgreSQL tutorial. It's very practical, showing complete example queries and their results.
    -
        ^merge: queries


    Joining multiple tables:
    -
        link: https://www.youtube.com/watch?v=TGt2xa7EzvI
        title: "SQL Join 3 Tables: How-To with Example"
        info: Video demonstrating why and how to use more than one `JOIN` in a query.
    -
        link: https://learnsql.com/blog/how-to-join-same-table-twice/
        title: How to Join the Same Table Twice
        info: This tutorial explains how you can `JOIN` the same table multiple times (bit with different `ON` conditions) in a single query.
    -
        link: https://stackoverflow.com/questions/7296846/how-to-implement-one-to-one-one-to-many-and-many-to-many-relationships-while-de
        title: Stack Overflow - How to implement one-to-one, one-to-many and many-to-many relationships while designing tables?
        info: |
            A couple of tables in our database (`film_actor`, `film_category`, `film_special_feature`) are so called *junction tables*: they establish a many-to-many relationship between two other tables. For instance, the `film_actor` table is used to associate each row in `film` with any number of rows `actor`, and vice versa. (A film can feature multiple actors. An actor can star in multiple films.)
            
            The answers to this Stack Overflow question provide good and to-the-point explanations of the different ways database tables can relate to each other, including through the use of a *junction table*.
    -
        ^merge: queries


    Counting:
    -
        link: https://www.postgresqltutorial.com/postgresql-aggregate-functions/postgresql-count-function/
        title: PostgreSQL Tutorial - PostgreSQL COUNT Function
        info: A text-based explanation of `COUNT`. You can skip the last two paragraphs, about `GROUP BY` and `HAVING`.
    -
        link: https://www.youtube.com/watch?v=q8O3IAeFaTE
        title: Using COUNT in Other Ways
        info: A video explaining roughly the same material.
    -
        ^merge: queries


    Left joins:
    -
        link: https://www.mysqltutorial.org/mysql-left-join.aspx
        title: MySQL LEFT JOIN
        info: An explanation of how to use the `LEFT JOIN` clause.
    -
        ^merge: queries
