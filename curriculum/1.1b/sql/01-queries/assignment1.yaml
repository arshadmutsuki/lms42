Introduction:

-
      link: https://www.youtube.com/watch?v=27axs9dO7AE
      title: What is SQL? [in 4 minutes for beginners]
-
      link: https://www.youtube.com/watch?v=YufocuHbYZo&list=PLi01XoE8jYojRqM4qGBF1U90Ee1Ecb5tt&index=5
      title: SQL SELECT Tutorial |¦| SQL Tutorial |¦| SQL for Beginners
      info: The SELECT statement in SQL is how you fetch data from your database.  In this SQL tutorial we give an introduction on how to write a SELECT query.  We will cover the SELECT, FROM, WHERE, ORDER BY, and LIMIT statements with many examples.

- |

      In this assignment we will work on using SQL to query data from our DVD rentals database. You will be asked to select, sort, filter and limit the data queried from the database.

How to work on the assignment: |
      Before you begin, you should use the *Add/Remove Software* tool from Manjaro to install the *postgresql-libs* and the *python-pip* package. (If you're using Mac OSX you can `brew install libpq virtualenv && brew link --force libpq`.)

      When you start working on the assignment by typing `lms open`, the file `assignment.md` will be opened using *Visual Studio Code*. For each of the objectives below, this file contains a couple of questions you will need to write an SQL query for.

      You can write your queries in place of the `TODO` placeholders. To execute your query, make sure the cursor is positioned within the SQL block and press `ctrl-shift-b`. A new bottom panel should be opened, showing you the results of your query, or any errors.

      Besides running your query and showing its results, `ctrl-shift-b` will also check if the results match the `Expected` clauses given below the SQL block. This is intended to help you spot and correct errors immediately, without having to await teacher feedback.

      Here's an example SQL block, for a query that lists all staff members:

      ```
            ```sql
            SELECT *
            FROM staff;
            ```
      ```

      The above query, as well as the examples from the following section, have been provided in the introduction section of the provided `assignment.md` file. Try it! You should see the query's results.


Studying the database schema: |
      In order to write SQL queries, you'll need to know which tables exist and what columns they have. We have the special `\d` command for that. In order to see a list of tables, you can run an SQL block like this:

      ```
            ```sql
            \d
            ```
      ```

      To see what columns a specific table has (as well as some other information about the table), you can add the table name to the `\d` command, like this:

      ```
            ```sql
            \d staff
            ```
      ```

      The output should look similar to:

      ```
      | column     | type                  | default                           |
      |------------|-----------------------|-----------------------------------|
      | id         | integer               | nextval('staff_id_seq'::regclass) |
      | first_name | character varying(45) | NULL                              |
      | last_name  | character varying(45) | NULL                              |
      | email      | character varying(50) | NULL                              |
      | store_id   | integer               | NULL                              |
      | active     | boolean               | true                              |
      | address    | character varying(80) | NULL                              |
      | city       | character varying(80) | NULL                              |
      | country    | character varying(80) | NULL                              |
      | district   | character varying(80) | NULL                              |

      * PRIMARY KEY (id) "idx_16403_staff_pkey"
      * FOREIGN KEY (store_id) REFERENCES store(id) "staff_store_id_fkey"
      ```

      You can ignore the rightmost (`default`) column of the output for now. The same applies for the `PRIMARY KEY` and `FOREIGN KEY` footnotes. They'll be introduced in later lessons. The `column` and `type` information will hopefully start to make sense soon though!


Field selection:
-
      link: https://www.w3schools.com/sql/sql_select.asp
      title: W3Schools - SQL SELECT Statement
      info: Explains the structure of the SQL SELECT statement used for querying data from the database.
-
      ^merge: queries
- |
      *Note:* If you don't understand what you're expected to do, please read the *How to work on the assignment* section again carefully. Still doesn't make sense? No worries, we understand that this part can be overwhelming. Please ask a class mate or a teacher to help you on your way.
      
Sort and limit:
-
      link: http://2015.padjo.org/tutorials/sql-basics/limit-and-order/
      title: LIMIT and ORDER BY in SQL Queries
      info: This article teaches you how to specify the quantity and arrangement of data rows returned by the SQL database.
- SQL coding conventions: |
      There are many different conventions for writing SQL statements. For the duration of this module, we want you to use the following convention:
            
      ```sql
      SELECT first_name, last_name
      FROM teacher
      WHERE level = 'Awesome'
      ORDER BY first_name
      LIMIT 2;
      ```

      The rules are:
      * You should put `SELECT`, `FROM`, `WHERE`, `ORDER BY` and `LIMIT` on a new line for clarity.
      * SQL keywords are often written in CAPITALS, to make them stand out from table and column names. If you prefer, it's okay to write them in lower case though. But please be consistent within a query.
-
      ^merge: queries

Search through the database:
-
      link: https://www.w3schools.com/sql/sql_where.asp
      title: SQL WHERE Clause
-
      link: https://www.w3schools.com/sql/sql_and_or.asp
      title: The SQL AND, OR and NOT Operators
-
      link: https://www.w3schools.com/sql/sql_like.asp
      title: SQL LIKE Operator
-
      ^merge: queries

One of a kind:
-
      link: https://www.w3schools.com/sql/sql_distinct.asp
      title: SQL SELECT DISTINCT Statement
-
      ^merge: queries
